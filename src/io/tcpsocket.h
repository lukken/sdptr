/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . TCP communication
* *********************************************************************** */

#ifndef TCPSOCKET_H
#define TCPSOCKET_H


/*
   TCP Server Socket listens to port p and binds to interface iface.
   Set iface="" to not to bind to an interface.
*/
class TCPSSocket {
  struct sockaddr_in name;
  struct hostent* hostinfo;
  int sock;
  uint16_t port;
  int MaxServers;

 public:
  TCPSSocket(uint16_t p, const char* ifname, int maxservers=1);
  ~TCPSSocket();
  int listen(void);
};

// Connected TCP server socket
class TCPCSSocket {
 protected:
    int sock;

 public:
    TCPCSSocket(int descr = -1);
    ~TCPCSSocket();
    void Shutdown() { shutdown(sock,SHUT_RDWR); }
    size_t rx(unsigned char* buf, size_t size);
    size_t _rx(unsigned char* buf, size_t size);
    size_t tx(const unsigned char* data, size_t size);

};

#endif // TCPSOCKET_H
