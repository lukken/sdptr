/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . Uniboard protocol
* *********************************************************************** */

#ifndef _REENTRANT
#error ACK! You need to compile with _REENTRANT defined since this uses threads
#endif

#include <cstring>
#include <cstdio>
#include <stdexcept>

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include "ucp.h"

#define ERROR_READ       1
#define ERROR_SEQCNT     2
#define ERROR_STATUS     3
#define ERROR_PACKETLEN  4

using namespace std;

extern int debug;

UCP::UCP(const string ipaddr) 
{
    commdev = new udpsocket(ipaddr, UNB_UDP_PORT);
    seq_nr = 0;
    count = 0;
}

UCP::~UCP()
{
    if (commdev != NULL) {
        delete commdev;
    }
}

bool UCP::TransmitRead(const uint32_t opcode, const uint32_t addr, const uint nvalues)
{
    size_t pktlength = sizeof(struct ucp_cmd_struct);
    uint8_t *pkt = new uint8_t[pktlength];
    memset(pkt, 0, pktlength);
    UnbosCmdPacket *unb = (UnbosCmdPacket*)pkt;

    seq_nr++;
    unb->hdr.psn = seq_nr;
    unb->hdr.opcode = opcode;
    unb->hdr.nvalues = nvalues;
    unb->hdr.addr = addr;

    //print_cmd_packet(pkt, pktlength);
    size_t retval = commdev->tx(pkt, pktlength);
    delete[] pkt;
    return (retval == pktlength); 
}


/*
 * Returns 0 if ok
 * Returns <0 if error 
 */
int UCP::ReceiveReadAck(uint32_t *buf, const uint nvalues)
{
    //cout << "UCP::ReceiveReadAck" << endl;
    int retstat = 0;
    size_t pktlength = sizeof(struct ucp_reply_struct) + nvalues * sizeof(uint32_t);
    uint8_t *pkt = new uint8_t[pktlength];
    memset(pkt, 0, pktlength);
    UnbosReadAckPacket *unb = (UnbosReadAckPacket*)pkt;

    size_t nbytes = commdev->rx(pkt, pktlength);
    if (nbytes != pktlength) {
        retstat = -ERROR_READ;
    } 
    else {
        if (unb->hdr.psn < seq_nr) {
            nbytes = commdev->rx(pkt, pktlength);
        }
        if (nbytes != pktlength) {
            retstat = -ERROR_READ;
        } 
        //print_reply_packet(pkt,(int)nbytes);
        if (unb->hdr.psn != seq_nr) {
            cerr << "UCP::ReceiveReadAck seq_nr NOT EQUAL !"
                      << " packet seq=" << unb->hdr.psn
                      << " class seq=" << seq_nr << endl;
            retstat = -ERROR_SEQCNT;
        }
    }
    if (retstat == 0) {
        memcpy(buf, unb->data, nvalues * sizeof(uint32_t));
    }
    delete[] pkt;
    //for (uint i=0;i<nvalues;i++) cout << (unsigned)buf[i] << " ";
    //cout << endl;
    return retstat;
}

void random_sleep(double tmax_in_sec) { 
    // cout << "random_sleep " << tmax_in_sec << endl; 
    usleep((int)((rand() * tmax_in_sec) / RAND_MAX * 1e6));
}

bool UCP::Read(const uint32_t opcode, const uint32_t addr, const uint nvalues, uint32_t *buf, const int max_retries)
{
    if (buf == NULL || nvalues == 0) {
        throw runtime_error("UCP::Read buf=NULL");
    }
    int retries = 0;
    int retstat = 0;
    uint nreceived = 0;

    //cout << "UCP::Read from addr=0x" << hex << addr << " span=" << dec << nvalues << endl;

    while (nreceived < nvalues) {
        uint _nvalues = nvalues - nreceived;
        if (_nvalues > UCP_BLOCK_SZ) {
            _nvalues = UCP_BLOCK_SZ;
        }

        uint32_t _addr = (addr);
        if (opcode == UCP_OPCODE_MEMORY_READ) {
            _addr += (nreceived * sizeof(uint32_t));
        }

        while (retries < max_retries) {
            TransmitRead(opcode, _addr, _nvalues);
            retstat = ReceiveReadAck(&buf[nreceived], _nvalues);
            if (retstat == 0) {
                break;
            }
            retries++;
            if (retries < max_retries) {
                if (retries == 1) {
                    random_sleep(0.05);
                }
                else if (retries == 2) {
                    random_sleep(0.5);
                }
            }
        }
        if (retries == max_retries) {
            break;
        }

        nreceived += _nvalues;
    }
    if (retries) {
        cerr << "UCP::Read() retries=" << retries << (retstat == 0 ? " (recovered)" : " (failed)") << endl;
    }
    if (retstat == 0) {
        return true; 
    }
    return false;
}


bool UCP::TransmitWrite(const uint32_t opcode, const uint32_t addr, const uint nvalues,
                        const uint32_t *buf)
{
    size_t pktlength = sizeof(struct ucp_cmd_struct) + nvalues*sizeof(uint32_t);
    uint8_t *pkt = new uint8_t[pktlength];
    memset(pkt, 0, pktlength);
    UnbosCmdPacket *unb = (UnbosCmdPacket*)pkt;

    unb->hdr.psn = ++seq_nr;
    unb->hdr.opcode = opcode;
    unb->hdr.nvalues = nvalues;
    unb->hdr.addr = addr;
    memcpy(unb->data, buf, nvalues*sizeof(uint32_t));

    //print_cmd_packet(pkt, pktlength);
    size_t retval = commdev->tx(pkt, pktlength);
    delete[] pkt;
    return (retval == pktlength); 
}

/*
 * Returns 0 if ok
 * Returns <0 if error 
 */
int UCP::ReceiveWriteAck(void)
{
    //cout << "UCP::ReceiveWriteAck" << endl;
    int retstat = 0;
    UnbosWriteAckPacket ack;

    size_t nbytes = commdev->rx((uint8_t*)&ack, sizeof(ack));
    if (nbytes != sizeof(ack)) { 
        retstat = -ERROR_READ;
    } 
    else {
        //print_reply_packet((const uint8_t *)&ack,(int)nbytes);
        if (ack.hdr.psn != seq_nr) {
            cerr << "UCP::ReceiveWriteAck seq_nr NOT EQUAL !"
                      << " packet seq=" << ack.hdr.psn
                      << " class seq=" << seq_nr << endl;
            retstat = -ERROR_SEQCNT;
        }
    }
    return retstat;
}

bool UCP::Write(const uint32_t opcode, const uint32_t addr, const uint nvalues,
                  const uint32_t *buf, const int max_retries)
{
    if (buf == NULL) {
        throw runtime_error("UCP::Write buf=NULL");
    }
    int retries = 0;
    int retstat = 0;
    uint nsent = 0;

    //cout << "UCP::Write to addr=0x" << hex << addr << " span=" << dec << nvalues << endl;

    while (nsent < nvalues) {
        uint _nvalues = nvalues-nsent;
        if (_nvalues > UCP_BLOCK_SZ) {
            _nvalues = UCP_BLOCK_SZ;
        }

        uint32_t _addr = (addr);
        if (opcode == UCP_OPCODE_MEMORY_WRITE) {
            _addr += (nsent * sizeof(uint32_t));
        }

        while (retries < max_retries) {
            TransmitWrite(opcode, _addr, _nvalues, &buf[nsent]);
            retstat = ReceiveWriteAck();
            if (retstat == 0) {
                break;
            }
            retries++;
            if (retries == 1) {
                random_sleep(0.05);
            }
            else if (retries == 2) {
                random_sleep(0.5);
            }
        }
        if (retries == max_retries) {
            break;
        }
        nsent += _nvalues;
    }
    if (retries) {
        cerr << "UCP::Write() retries=" << retries << (retstat == 0 ? " (recovered)" : " (failed)") << endl;
    }
    if (retstat == 0) {
        return true; 
    }
    return false;
}

void UCP::print_cmd_packet(const uint8_t *pkt, const int len)
{
    cout << "\x1b[31;1m" << "UCP::print_cmd_packet" << "\x1b[0m" << endl;
    UnbosCmdPacket *pkt_ptr=(UnbosCmdPacket *)pkt;
    cout << " hdr.psn=" << pkt_ptr->hdr.psn
         << " hdr.opcode=" << pkt_ptr->hdr.opcode
         << " hdr.nvalues=" << dec << pkt_ptr->hdr.nvalues
         << " hdr.addr=" << pkt_ptr->hdr.addr << endl;

    const uint8_t *bufptr = pkt;
    cout << "       ";
    for (int i=0; i<16; i++) {
        cout << setw(2) << right << hex << i << " ";
    }
    for (int i=0; i<len; i++) {
        if ((i % 16) == 0) {
            cout << endl << setfill('0') << setw(4) << right << hex << i << " : ";
        }
        //cout << setfill('0') << setw(2) << right << hex << bufptr[i] << " ";
        cout << setfill('0') << setw(2) << right << hex << (int)*bufptr << " ";
        bufptr++;
    }
    cout << endl;
}

void UCP::print_reply_packet(const uint8_t *pkt, const int len)
{
    cout << "\x1b[32;1m" << "UCP::print_reply_packet" << "\x1b[0m" << endl;
    UnbosReadAckPacket *pkt_ptr = (UnbosReadAckPacket *)pkt;
    cout << " hdr.psn=" << pkt_ptr->hdr.psn
         << " hdr.addr=" << pkt_ptr->hdr.addr << endl;

    const uint8_t *bufptr = pkt;
    cout << "       ";
    for (int i=0; i<16; i++) {
        cout << setfill('0') << setw(2) << right << hex << i << " ";
    }
    for (int i=0; i<len; i++) {
        if ((i % 16) == 0) {
            cout << endl << setfill('0') << setw(4) << right << hex << i << " : ";
        }
        cout << setfill('0') << setw(2) << right << hex << (int)*bufptr << " ";
        bufptr++;
    }
    cout << endl;
}

bool UCP::readRegister(uint32_t addr, uint32_t nvalues, uint32_t *data_ptr, const bool isfifo, const int max_retries)
{
    uint32_t opcode;
    if (data_ptr == NULL) {
        throw runtime_error("UCP::readRegister data_ptr=NULL");
    }
    if (isfifo) {
        opcode = UCP_OPCODE_FIFO_READ;
    } 
    else {
        opcode = UCP_OPCODE_MEMORY_READ;
    }
    return Read(opcode, addr, nvalues, data_ptr, max_retries); 
}

bool UCP::writeRegister(uint32_t addr, uint32_t nvalues, const uint32_t *data_ptr, const bool isfifo, const int max_retries)
{
    uint32_t opcode;
    if (data_ptr == NULL) {
        throw runtime_error("UCP::writeRegister data_ptr=NULL");
    }
    if (isfifo) {
        opcode = UCP_OPCODE_FIFO_WRITE;
    } 
    else {
        opcode = UCP_OPCODE_MEMORY_WRITE;
    }
    return Write(opcode, addr, nvalues, data_ptr, max_retries);
}

