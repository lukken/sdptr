/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . This is the class that does the communication with the nodes, using several
#   threads with workers.
* *********************************************************************** */

#ifndef NODE_H
#define NODE_H


#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <array>

#include "io/ucp.h"
#include "registers.h"
#include "periph/fpga.h"
#include "tools/message_queue.h"


class NODE_config {
public:
    int gn;
    std::string ipaddr;
    int n_beamsets;
};

#define MAX_DATA_SIZE 32800  // Size of data array in cCmd and cReply.
#define MAX_ADDR_SIZE 128    // Size of rel_addr in cCmd.
#define MAX_TYPE_SIZE 8      // Size of rel_addr in cCmd.

/*
* cCmd class used for sending messages to the worker thread.
* init() is used to clear all values.
*/
class cCmd {
public:
   cCmd();
   ~cCmd();
   void init();
   char cmdId;                          // Requested cmd: 'R', 'W', 'S' or 'M' task. (Read, Write, Monitor or Stop).
   char relative_addr[MAX_ADDR_SIZE];   // fpga register address to access.
   char type[MAX_TYPE_SIZE];            // "fpga" or "mm" task. (fpga = function, mm = direct).
   uint nvalues;                        // Number of values of type format in data.
   int format;                          // data format in data, see REG_FORMAT_* in register.h for all available types
   char data[MAX_DATA_SIZE];            // data array.
};

/*
* cReply class used for return value from the worker thread.
* init() is used to clear all values.
*/
class cReply {
public:
   cReply();
   ~cReply();
   void init();
   char cmdId;                // processed cmd: 'R', 'W', 'S' or 'M' task. (Read, Write, Monitor or Stop)
   uint retval;               // return value of executed cmd, true = success, false = failure.
   char data[MAX_DATA_SIZE];  // data array.
   int nvalues;               // Number of values of type format in data.
   int format;                // data format in data, see REG_FORMAT_* in register.h
};


#define GLOBALNODE_to_UNB(n)   (n>>2)
#define GLOBALNODE_to_TYPE(n)  ("pn")
#define GLOBALNODE_to_NODE(n)  (n&0x3)

const int32_t cmd_buf_size = 35000;
const int32_t reply_buf_size = 35000;

class Node {
 private:
  std::string myIPaddr;
  uint UniboardNr;
  uint LocalNr;
  uint GlobalNr;
  uint n_beamsets;
  std::string Type;

  std::thread *worker_thread;
  CMessageQueue<cCmd> cmd_queue;
  CMessageQueue<cReply> reply_queue;

  Periph_fpga *periph_fpga;

  void print_node_nr(void) {
    std::cout << "Uniboard=" << UniboardNr << " type=" << Type
              << " localnr=" << LocalNr << " globalnr=" << GlobalNr << std::endl;
  }

 public:
  Node(const std::string ipaddr, const uint unb, const uint localnr, const std::string type, const uint nof_beamsets);
  ~Node();

  uint ipaddr_to_id(const std::string ipaddr);
  const uint GetLocalNr()    { return LocalNr; }
  const uint GetUniboardNr() { return UniboardNr; }
  const std::string GetIP()  { return myIPaddr; }
  const uint GetGlobalNr()   { return GlobalNr; }
  const uint GetNr()         { return LocalNr; }
  const std::string GetType() { return Type; }
  bool isOnline(void);
  bool isMasked();
  void setMasked(bool mask);
  uint32_t ppsOffsetCnt(void);

  bool monitor(TermOutput& t);

  void worker();
  bool exec_cmd(const char cmd, const std::string relative_addr,
                const std::string type, const char *data,
                const int nvalues, const int format);
  bool exec_reply(TermOutput& t);

  CMMap * get_MMap(void);
};

#endif /* NODE_H */
