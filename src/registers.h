/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . register class used to hold register information
* *********************************************************************** */

#ifndef __REGISTERMAP_H__
#define __REGISTERMAP_H__

#include <string>
#include <map>
#include <vector>

#define SIZE1STRING 100

#define REG_ADDR_SYSTEM_INFO         (0x0)
#define REG_ADDR_SYSTEM_INFO_SPAN      (1)
#define REG_ADDR_ROM_SYSTEM      (0x10000)
#define REG_ADDR_ROM_SYSTEM_SPAN    (8192)


#define REG_FORMAT_UNKNOWN  0
#define REG_FORMAT_BOOLEAN  1
#define REG_FORMAT_INT8     2
#define REG_FORMAT_UINT8    3
#define REG_FORMAT_INT16    4
#define REG_FORMAT_UINT16   5
#define REG_FORMAT_INT32    6
#define REG_FORMAT_UINT32   7
#define REG_FORMAT_INT64    8
#define REG_FORMAT_UINT64   9
#define REG_FORMAT_FLOAT    10
#define REG_FORMAT_DOUBLE   11
#define REG_FORMAT_STRING   12

int32_t reg_format_size_in_bytes(int32_t type);
int32_t reg_format_size_in_words(int32_t type);


class CMMap {
private:

  typedef struct {
    uint32_t base;
    uint32_t n_mm_ports;
    uint32_t span;
    uint32_t mask;
    uint32_t shift;
    std::string access;
    std::string type;
    uint32_t mm_port_span;
    uint32_t peripheral_span;
  } register_info;

  std::map <std::string, register_info> reg;

public:
   CMMap();
   ~CMMap();
   bool empty() { return reg.empty(); }
   void clear() { reg.clear(); }
   bool add_register(const std::string name, const uint32_t base, const uint32_t n_mm_ports, const uint32_t span,
                     const uint32_t mask, const uint32_t shift, const std::string access,
                     const std::string type, const uint32_t peripheral_span, const uint32_t mm_port_span);
   bool update_register(const std::string name, const uint32_t base, const uint32_t n_mm_ports, const uint32_t span,
                     const uint32_t mask, const uint32_t shift, const std::string access,
                     const std::string type, const uint32_t peripheral_span, const uint32_t mm_port_span);

   bool find_register(std::string name);
   void print(std::ostringstream& strs, std::string prefix);
   void print_screen(void);

   uint32_t getBaseAddr(const std::string name) { return reg[name].base; }
   uint32_t getSpan(const std::string name) { return reg[name].span; }
   std::string getPerm(const std::string name) { return reg[name].access; }
   uint32_t getMask(const std::string name) { return reg[name].mask; }
   uint32_t getShift(const std::string name) { return reg[name].shift; }
   std::string getType(const std::string name) { return reg[name].type; }
   uint32_t getNPorts(const std::string name) { return reg[name].n_mm_ports; }
   uint32_t getPortSpan(const std::string name) { return reg[name].mm_port_span; }
   uint32_t getPeripheralSpan(const std::string name) { return reg[name].peripheral_span; }

   bool type_isfifo(const std::string name) { return (reg[name].type == "FIFO"); }
   uint32_t getAddr(const std::string name);
   uint32_t getValidAddr(const std::string name,
                         const uint32_t size);
   bool     getReadPermission(const std::string name);
   bool     getWritePermission(const std::string name);
   void     setPermission_NA(const std::string name) { reg[name].access = "NA"; }
   void     setAllPermission_NA(void);

   std::vector<std::string> getRegnames(std::string prefix);
   std::vector<std::string> getRegnames_full(std::string prefix);
};


class CPointMap {
private:

  typedef struct {
    std::string relname;
    uint32_t n_nodes;
    uint32_t n_data;
    std::string access;
    uint32_t format;
  } register_info;

  std::map <std::string, register_info> reg;

 public:
   CPointMap();
   ~CPointMap();

   bool add_register   (const std::string name, const std::string relname, const uint32_t n_nodes, const uint32_t n_data,
                        const std::string access, const uint32_t format);
   bool update_register(const std::string name, const std::string relname, const uint32_t n_nodes, const uint32_t n_data,
                        const std::string access, const uint32_t format);
   bool find_register(std::string name);
   void print(std::ostringstream& strs, std::string prefix);
   void print_screen(void);

   std::string getRelativeName(const std::string name) {return reg[name].relname; }
   uint32_t getNodesSize(const std::string name) { return reg[name].n_nodes; }
   uint32_t getDataSize(const std::string name) { return reg[name].n_data; }
   std::string getPerm(const std::string name) { return reg[name].access; }
   uint32_t getFormat(const std::string name) { return reg[name].format; }
   bool getReadPermission(const std::string name);
   bool getWritePermission(const std::string name);
   void setPermission_NA(const std::string name) { reg[name].access = "NA"; }
   void setAllPermission_NA(void);

   std::vector<std::string> getRegnames(std::string prefix);
};

#endif // __REGISTERMAP_H__
