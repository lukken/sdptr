/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . this is the opc_ua server
* *********************************************************************** */

#include <open62541/plugin/log_stdout.h>
#include <open62541/server.h>
#include <open62541/server_config_default.h>

#include <iostream>
#include <cstring>
#include <cstdio>
#include <csignal>
#include <cstdlib>
#include <stdexcept>

#include "ua_server.h"

using namespace std;

#define UA_LOFAR_NAMESPACE "http://lofar.eu"

extern volatile bool ServerRunning;
extern Serverdat SD;

UA_Server *mUaServer;
UA_UInt16 mUaLofarNameSpace;

/*
 * Links of interest:
 * Datatypes: https://open62541.org/doc/current/types.html
 *            https://open62541.org/doc/current/tutorial_datatypes.html
 *            https://open62541.org/doc/0.1/group__types.html
 *            https://open62541.org/doc/current/types_generated.html?highlight=ua_datatype
 * Special Server functions: https://open62541.org/doc/0.3/server.html
 *                           https://open62541.org/doc/0.2/server.html
 * About nodes: https://open62541.org/doc/current/nodestore.html
 */

// for debug only:
void ua_print_nodeid(UA_NodeId *nd)
{
    // note: this function complains:
    // *** Error in `./sdptr': free(): invalid pointer: 0x00007ffc2e7da720 ***
    // --> must be a bug in UA_NodeId_print()
    UA_String ua_str;
    cout << "ua_print_nodeid:" << endl;
    UA_NodeId_print(nd, &ua_str);

    char *str = new char[ua_str.length + 1];
    memcpy((void *)str, (void *)ua_str.data, ua_str.length);
    str[ua_str.length] = 0; // add end of string char
    cout << str << endl;
    delete[] str;
}


/*
* ua_read_DataSource(), this function is called by the opc-ua server to read a point.
*/
static UA_StatusCode ua_read_DataSource(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                UA_Boolean sourceTimeStamp, const UA_NumericRange *range,
                UA_DataValue *dataValue)
{
    char *regname = new char[nodeId->identifier.string.length + 1];
    memcpy((void *)regname, (void *)nodeId->identifier.string.data, nodeId->identifier.string.length);
    regname[nodeId->identifier.string.length] = 0; // add end of string char

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "ua_read_DataSource: reading from %s",regname);

    UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname);
    UA_NodeId ntype;
    //UA_StatusCode st =
    UA_Server_readDataType(server, currentNodeId, &ntype);
    // assert(st == UA_STATUSCODE_GOOD);
    //printf("update_FPGA_Variable nodeid %s ns=%d DataType=%d numeric=%d\n",(char *)regname.c_str(),
    //       ntype.namespaceIndex,ntype.identifierType,ntype.identifier.numeric);
    // output: update_FPGA_Variable nodeid FPGA_version_R ns=0 DataType=0 numeric=12
    //ua_print_nodeid(&ntype);
    // output: i=12

    // ntype.identifier.numeric is the datatype e.g. for UA_Float=10, UA_String=12
    //printf("update_FPGA_Variable UA_TYPES_FLOAT=%d numeric=%d\n",UA_TYPES_FLOAT,ntype.identifier.numeric);
    // output: update_FPGA_Variable UA_TYPES_FLOAT=9 numeric=10
    // weird...  must do -1
    // see: https://open62541.org/doc/current/types.html

    TermOutput termout;

    // read values for point
    try {
        // Translator regnames start with "TR_" others "FPGA_"
        if (strncmp(regname,"TR_", 3) == 0) {
            // UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"ua_read_DataSource: for translator class");
            SD.tr->read(termout, regname, -1);
        }
        else {
            // UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"ua_read_DataSource: for fpga class");
            SD.unb->read(termout, regname, -1);
        }
    } catch (runtime_error& e) {
        cerr << "ua_read_DataSource error: " << e.what() << endl;
    }

    
    uint type = ntype.identifier.numeric - 1;
    uint32_t sz;
    // if scalar (only TR_* points)
    if (termout.nof_vals == 1) {
        if (type == UA_TYPES_STRING) {
            char *ptr = (char *)termout.val;
            UA_String value = UA_STRING_ALLOC((char *)ptr);
            UA_Variant_setScalarCopy(&dataValue->value, &value, &UA_TYPES[UA_TYPES_STRING]);
            dataValue->hasValue = true;
        }
        else {
            UA_Variant_setScalarCopy(&dataValue->value, termout.val, &UA_TYPES[type]);
            dataValue->hasValue = true;
        }
    }
    // else array
    else {
        if (type == UA_TYPES_STRING) {
            UA_String *values = (UA_String *) UA_Array_new(termout.nof_vals, &UA_TYPES[UA_TYPES_STRING]);
            char *ptr = (char *)termout.val;
            for (unsigned int i=0; i<termout.nof_vals; i++) {
                values[i] = UA_STRING_ALLOC((char *)ptr);
                ptr += SIZE1STRING;
            }
            UA_Variant_setArray(&dataValue->value, values, termout.nof_vals, &UA_TYPES[UA_TYPES_STRING]);
            dataValue->hasValue = true;
        }
        else {
            void * values = UA_Array_new(termout.nof_vals, &UA_TYPES[type]);
            
            sz = termout.nof_vals * UA_TYPES[type].memSize;
            memcpy(values, termout.val, sz);
            UA_Variant_setArray(&dataValue->value, values, termout.nof_vals, &UA_TYPES[type]);
            dataValue->hasValue = true;
        }

    }
   
    delete[] regname;
    return UA_STATUSCODE_GOOD;
}

/*
* ua_write_DataSource(), this function is called by the opc-ua server to write a point.
*/
static UA_StatusCode ua_write_DataSource(UA_Server *server,
                 const UA_NodeId *sessionId, void *sessionContext,
                 const UA_NodeId *nodeId, void *nodeContext,
                 const UA_NumericRange *range, const UA_DataValue *data)
{
    bool retval = false;
    char *regname = new char[nodeId->identifier.string.length + 1];
    memcpy((void *)regname, (void *)nodeId->identifier.string.data, nodeId->identifier.string.length);
    regname[nodeId->identifier.string.length] = 0; // add end of string char

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "ua_write_DataSource: writing to %s",regname);

    UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname);
    UA_NodeId ntype;
    UA_Server_readDataType(server, currentNodeId, &ntype);

    // TODO: can this be removed.
    // UA_Variant nv_dims;
    // https://open62541.org/doc/0.2/server.html 
    // UA_Server_readArrayDimensions(server, currentNodeId, &nv_dims);

    // check if there is data to write, if not retval remains false.
    if (data->hasValue && data->value.arrayLength > 0) {
        //data.value is of type UA_Variant.
        uint type = ntype.identifier.numeric - 1;
        char *data_sdp;
        uint32_t sz;
        if (type == UA_TYPES_STRING) {
            sz = data->value.arrayLength * SIZE1STRING;
            data_sdp = new char[sz];
            UA_String *dptr = (UA_String *)data->value.data;
            UA_String str_data;
            for (int i=0; i<(int)data->value.arrayLength; i++){
                str_data = dptr[i];
                memcpy((void *)&(data_sdp[i*SIZE1STRING]), (void *)str_data.data, str_data.length);
            }
            retval = true;
        }
        else {
            try {
                sz = data->value.arrayLength * UA_TYPES[type].memSize;
                data_sdp = new char[sz];
                memcpy((void *)data_sdp, (void *)data->value.data, sz);
                retval = true;
            } catch (runtime_error& e) {
                cerr << "ua_write_DataSource error: " << e.what() << endl;
                retval = false;
            }
        }

        if (retval) {
            TermOutput termout;
            try {
                if (strncmp(regname,"TR_",3) == 0) {
                    // UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"ua_write_DataSource: for translator class");
                    SD.tr->write(termout, regname, data_sdp, data->value.arrayLength);
                }
                else {
                    // UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,"ua_write_DataSource: for fpga class");
                    SD.unb->write(termout, regname, data_sdp, data->value.arrayLength);
                }
            } catch (runtime_error& e) {
                cerr << "ua_write_DataSource error: " << e.what() << endl;
                retval = false;
            }
        }
        else {
            cerr << "ua_write no retval" << endl;
        }
        if (data_sdp != NULL) { delete[] data_sdp; }
    }
    delete[] regname;
    return UA_STATUSCODE_GOOD;
}


/*
* ua_add_Variable_[data_type]() functions to add a UA_Variant variabel (point) to the server
*/

static void ua_add_Variable_string(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_string: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_String value = UA_STRING_ALLOC((char *)"unknown");
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_STRING]);
        vattr.valueRank = UA_VALUERANK_SCALAR;

    }
    else {
        UA_String *values = (UA_String *) UA_Array_new(size, &UA_TYPES[UA_TYPES_STRING]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_STRING_ALLOC((char *)"unknown");
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_STRING]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }

}

static void ua_add_Variable_int64(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_int64: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Int32 value = UA_Int64(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_INT64]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_Int64 *values = (UA_Int64 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_INT64]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Int64(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_INT64]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_INT64].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_uint64(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_uint64: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_UInt64 value = UA_UInt64(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_UINT64]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_UInt64 *values = (UA_UInt64 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_UINT64]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_UInt64(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_UINT64]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_UINT32].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_int32(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_integer: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Int32 value = UA_Int32(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_INT32]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_Int32 *values = (UA_Int32 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_INT32]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Int32(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_INT32]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_uint32(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_uint32: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_UInt32 value = UA_UInt32(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_UINT32]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_UInt32 *values = (UA_UInt32 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_UINT32]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_UInt32(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_UINT32]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_UINT32].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_int16(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_int16: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Int16 value = UA_Int16(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_INT16]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_Int16 *values = (UA_Int16 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_INT16]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Int16(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_INT16]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_INT16].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_uint16(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_uint16: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_UInt16 value = UA_UInt16(0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_UINT16]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        UA_UInt16 *values = (UA_UInt16 *) UA_Array_new(size, &UA_TYPES[UA_TYPES_UINT16]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Int16(0);
        }
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_UINT16]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_UINT16].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_float(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_float: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Float value = UA_Float(0.0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_FLOAT]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Float *values = (UA_Float *) UA_Array_new(size, &UA_TYPES[UA_TYPES_FLOAT]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Float(0.0);
        }
        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_FLOAT]);

        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());
    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_double(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_double: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Double value = UA_Double(0.0);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_DOUBLE]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Double *values = (UA_Double *) UA_Array_new(size, &UA_TYPES[UA_TYPES_DOUBLE]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Double(0.0);
        }
        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_DOUBLE]);

        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_DOUBLE].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());
    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}

static void ua_add_Variable_boolean(UA_Server *server, string regname, unsigned int size, string perm, bool is_scalar)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "ua_add_Variable_boolean: %s", (char *)regname.c_str());
    UA_VariableAttributes vattr = UA_VariableAttributes_default;

    if (perm == "RW") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "WO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE;
    }
    else if (perm == "RO") {
        vattr.accessLevel = UA_ACCESSLEVELMASK_READ;
    }

    if (is_scalar) {
        UA_Boolean value = UA_Boolean(false);
        UA_Variant_setScalar(&vattr.value, &value, &UA_TYPES[UA_TYPES_BOOLEAN]);
        vattr.valueRank = UA_VALUERANK_SCALAR;
    }
    else {
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        UA_UInt32 myArrayDimensions[1] = {size};
        vattr.arrayDimensions = myArrayDimensions;
        vattr.arrayDimensionsSize = 1;

        UA_Boolean *values = (UA_Boolean *) UA_Array_new(size, &UA_TYPES[UA_TYPES_BOOLEAN]);
        for (unsigned int i=0; i<size; i++) {
            values[i] = UA_Boolean(false);
        }

        UA_Variant_setArray(&vattr.value, values, size, &UA_TYPES[UA_TYPES_BOOLEAN]);
        vattr.value.arrayDimensions = myArrayDimensions;
        vattr.value.arrayDimensionsSize = 1;
    }

    vattr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(server,
                                       UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), // parentNodeId
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES), // parentReferenceNodeId
                                       UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
                                       UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                       vattr, DataSource, NULL, NULL);
    if (retval != UA_STATUSCODE_GOOD) {
        // handle error
        cout << "Error adding node" << endl;
    }
}
/* end of ua_add_Variable_[data_type]() functions */


/*
*  ua_add_Variable(), main function to add variabel(point) for given format(data_type) to 
*  the opc-ua server by calling an ua_add_Variable_[data_type]() functions.
*  if data size is 1, its a scalar.
*/
static void ua_add_Variable(UA_Server *server, string regname, int format, unsigned int size, string perm)
{
    cout << "regname=" << regname << " format=" << format << " size=" << size << endl;
    bool is_scalar = (size == 1);

    switch (format) {
        case REG_FORMAT_STRING: {
            ua_add_Variable_string(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_INT64: {
            ua_add_Variable_int64(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_UINT64: {
            ua_add_Variable_uint64(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_INT32: {
            ua_add_Variable_int32(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_UINT32: {
            ua_add_Variable_uint32(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_INT16: {
            ua_add_Variable_int16(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_UINT16: {
            ua_add_Variable_uint16(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_FLOAT: {
            ua_add_Variable_float(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_DOUBLE: {
            ua_add_Variable_double(server, regname, size, perm, is_scalar);
        } break;
        case REG_FORMAT_BOOLEAN: {
            ua_add_Variable_boolean(server, regname, size, perm, is_scalar);
        } break;
        default: {
            cout << "Unknown format [" << format << "] not yet supported" << endl;
            return; // not yet supported
        } break;
    }
}

/*
* ua_server_init(), get all FPGA_ and TR_ poins and add them to the opc-ua server.
*
* SD.unb->get_pointMap()     -->  FPGA_*  (points defined in src/fpga.cpp)
* SD.tr->getTranslatorMap()  -->  TR_*    (points defined in src/tr.cpp)
*/
int ua_server_init(bool warm_start)
{
    if (!warm_start) {
        mUaServer = UA_Server_new();
        UA_ServerConfig_setDefault(UA_Server_getConfig(mUaServer));

        mUaLofarNameSpace = UA_Server_addNamespace(mUaServer, UA_LOFAR_NAMESPACE);
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "UA Server ns=%d for %s\n",
                    mUaLofarNameSpace,UA_LOFAR_NAMESPACE);

        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "UA Server add pointMap");
        CPointMap *pointmap = SD.unb->get_pointMap();
        vector<string> regnames = pointmap->getRegnames("");
        for (auto m : regnames) {
            int format = pointmap->getFormat(m);
            unsigned int size = pointmap->getNodesSize(m) * pointmap->getDataSize(m);
            string perm = pointmap->getPerm(m);
            ua_add_Variable(mUaServer, m, format, size, perm);
        }

        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "UA Server add translatorMap");
        CPointMap *trmap = SD.tr->getTranslatorMap();
        vector<string> trregnames = trmap->getRegnames("");
        for (auto m : trregnames) {
            int format = trmap->getFormat(m);
            unsigned int size = trmap->getNodesSize(m) * trmap->getDataSize(m);
            string perm = trmap->getPerm(m);
            ua_add_Variable(mUaServer, m, format, size, perm);
        }
    }
    cout << "sizeof bool=" << UA_TYPES[UA_TYPES_BOOLEAN].memSize << endl;
    cout << "sizeof int16=" << UA_TYPES[UA_TYPES_INT16].memSize << endl;
    cout << "sizeof int32=" << UA_TYPES[UA_TYPES_INT32].memSize << endl;
    cout << "sizeof float=" << UA_TYPES[UA_TYPES_FLOAT].memSize << endl;
    cout << "sizeof double=" << UA_TYPES[UA_TYPES_DOUBLE].memSize << endl;
    return 0;
}

/*
* ua_server(), start opc-ua server, called from sdptr.cpp
*/
int ua_server(void)
{
    UA_StatusCode retval = UA_Server_run(mUaServer, &ServerRunning);

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "UA Server Stopped");
    UA_Server_delete(mUaServer);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}

