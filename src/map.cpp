/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */

#include "map.h"
#include "sdptr.h"

using namespace std;

extern int debug;


UniboardMap::UniboardMap(list<class Node*>& nodelist, const int32_t n_beamsets) : Board(nodelist), Fpga(nodelist, n_beamsets)
{
    initialized = false;
}


bool UniboardMap::init(TermOutput& termout)
{
    throw runtime_error("map init not executed");
    bool retval = false;
    initialized = false;

// add all registers

    if (retval) initialized = true;
    return retval;
}

bool UniboardMap::read(TermOutput& termout,
                       const string addr,
                       const int nvalues)
{
    bool retval = false;
    if (is_fpga(addr)) { // addressed with FPGA_...
        //cout << "UniboardMap::read fpga application" << endl;
        retval = point(termout, 'R', addr, NULL, nvalues);
    }
    return retval;
}

bool UniboardMap::write(TermOutput& termout,
                        const string addr,
                        const char *data,
                        const int nvalues)
{
    bool retval = false;
    if (is_fpga(addr)) { // addressed with FPGA_...
        //cout << "UniboardMap::write fpga point [" << addr << "]" << endl;
        retval = point(termout, 'W', addr, data, nvalues);
    }
    return retval;
}
