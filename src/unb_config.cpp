/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . 
* *********************************************************************** */

#include <iostream>
#include <sstream>
#include <string>
#include <boost/regex.hpp>

#include "unb_config.h"

using namespace std;

bool importdatfile(ifstream& ifs, list<class NODE_config>& N)
{
    NODE_config nc;
    ostringstream err_str;

#define REGEXP_MAC "([0-9a-fA-F]{1,2}[:-]){5}[0-9a-fA-F]{1,2}"
#define REGEXP_IP  "([0-9]{1,3}\\.){3}[0-9]{1,3}"

//                                     tag       gn      ipaddr
#define REGEXP_DATFILE_LINE_NODE "^\\s*node\\s+\\d+\\s+" REGEXP_IP ".*$"

/*
            nc.gn = 2;
            nc.ipaddr = "10.99.2.3";
            nc.firmware = "unb2b_minimal";
            nc.version = 2;
            N.push_back(nc);

            nc.gn = 3;
            nc.ipaddr = "10.99.2.4";
            nc.firmware = "unb2b_minimal";
            nc.version = 2;
            N.push_back(nc);
*/
    boost::regex comment_re, spaces_re, node_re;

    boost::cmatch matches;
    comment_re =  string("^\\s*#+.*$");
    spaces_re = string("^\\s*$");
    node_re = string(REGEXP_DATFILE_LINE_NODE);

    char line[150];
    while (ifs.getline(line, sizeof(line))) {

        if (regex_match(line, comment_re)) {
            //cout << "comment ok" << endl;
        } 
        else if (regex_match(line, spaces_re)) {
            //cout << "spaces ok" << endl;
        }
        else if (regex_match(line, node_re)) { // NODE conf
            stringstream strs(line);
            string prefx;
            strs >> prefx;
            strs >> nc.gn;
            strs >> nc.ipaddr;
            strs >> nc.firmware;
            strs >> nc.version;
            strs >> nc.enabled;

            if (!(strs.fail() || strs.bad())) {
                N.push_back(nc);
            }
        }
        else {
            cerr << "importdatfile: error! illegal line: [" << line << "]" << endl;
        }
    }

    if (N.size() == 0) {
        cerr << "no node entries" << endl;
        return false;
    }
    return true;
}


/*
 * Some help on REGEX's
 *
 *

Symbol  Meaning
c       Match the literal character c once, unless it is one of the special characters.
^       Match the beginning of a line.
.       Match any character that isn't a newline.
$       Match the end of a line.
|       Logical OR between expressions.
()      Group subexpressions.
[]      Define a character class.
*       Match the preceding expression zero or more times.
+       Match the preceding expression one ore more times.
?       Match the preceding expression zero or one time.
{n}     Match the preceding expression n times.
{n,}    Match the preceding expression at least n times.
{n, m}  Match the preceding expression at least n times and at most m times.
\d      Match a digit.
\D      Match a character that is not a digit.
\w      Match an alpha character, including the underscore.
\W      Match a character that is not an alpha character.
\s      Match a whitespace character (any of \t, \n, \r, or \f).
\S      Match a non-whitespace character.
\t      Tab.
\n      Newline.
\r      Carriage return.
\f      Form feed.
\m      Escape m, where m is one of the metacharacters described above: ^, ., $, |, (), [], *, +, ?, \, or /.
*/

