/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . class with fpga registers available for opc-ua server
* *********************************************************************** */

#ifndef FPGA__H
#define FPGA__H 1

#include <string>
#include <vector>
#include <list>

#include "node.h"
#include "tools/util.h"
#include "registers.h"


class Fpga
{
private:
    int32_t nBeamsets;
    std::list<class Node*> FPGA;
    CPointMap *pointMap;

public:
    Fpga(std::list<class Node*>& nodelist, const int32_t n_beamsets);
    ~Fpga();

    bool point(TermOutput& termout, const char cmd, const std::string addr,
               const char *data, const int len);

    Node * select_node(const int nr);
    std::vector<int> get_all_nodes(void);
    std::vector<bool> get_all_enabled_nodes(void);
    std::vector<bool> get_all_masked_nodes(void);
    std::vector<bool> get_all_offline_nodes(void);
    void set_all_masked_nodes(std::vector<bool> masked);
    std::vector<uint32_t> get_all_pps_offset_cnt(void);

    CPointMap * get_pointMap(void);
    uint node_number(Node *node);
    bool is_fpga(const std::string addr);
};
#endif

