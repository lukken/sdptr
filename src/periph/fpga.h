/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . class for fpga registers (peripherals) that need some more actions
* *********************************************************************** */

#ifndef __PERIPH_FPGA_H__
#define __PERIPH_FPGA_H__


#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "../io/ucp.h"
#include "../tools/util.h"
#include "../registers.h"
#include "../constants.h"


class Periph_fpga {
private:
  Tictoc tictoc;  // used to get some timing information
  UCP *ucp;
  CMMap *mmap;
  uint32_t GlobalNr;
  uint32_t nBeamsets;

  // Masked is set by the client and is used to set the fpga's to communinicate with, true is communicate, false do not.
  bool Masked;
  // Online is the communication state with the fpga true if no errors, false is no communication.
  // state is set in read_system_info() function that is called every second from the monitor() function.
  bool Online;

  std::string my_current_design_name;
  uint my_current_hw_version;
  std::string my_current_fw_version;

  bool    my_bsn_input_sync_timeout;
  int64_t my_bsn_input_bsn;
  int32_t my_bsn_input_nof_packets;
  int32_t my_bsn_input_nof_valid;
  int32_t my_bsn_input_nof_err;

  uint32_t my_jesd_csr_rbd_count[C_S_pn];
  uint32_t my_jesd_csr_dev_syncn[C_S_pn];
  uint32_t my_jesd_rx_err0[C_S_pn];
  uint32_t my_jesd_rx_err1[C_S_pn];
  bool    my_xst_processing_enable;
  uint32_t my_pps_offset_cnt;

  double my_signal_input_mean[C_S_pn];
  double my_signal_input_rms[C_S_pn];

  std::ofstream rbf_wf;
  uint32_t Flash_page_start, Flash_page_size_bytes, Flash_user_sector_start,
           Flash_user_sector_end, Flash_pages_per_sector,
           Flash_fact_sector_start, Flash_fact_sector_end, Flash_select;

  bool Read(const std::string addr_str, uint32_t *data_ptr, bool use_mask_shift);
  bool Write(const std::string addr_str, uint32_t *data_ptr, bool use_shift_mask);
  uint32_t mask_shift(const std::string addr_str, uint32_t data);
  uint32_t shift_mask(const std::string addr_str, uint32_t data);

  bool flash_erase_sector(uint32_t sector);
  bool flash_erase();
  bool flash_pages(const char *data, const uint len);
  bool flash_page(const char *data);
  bool flash_prot(const char *data);
  bool wait_while_epcs_busy(uint sleeptime);
  std::string read_design_name();
  std::string read_design_note();

  bool read_system_info(TermOutput& termout);
  bool read_hardware_version(TermOutput& termout, int format);
  bool read_firmware_version(TermOutput& termout, int format);
  bool read_stamps(TermOutput& termout, int format);

  bool read_global_node_index(TermOutput& termout, int format);

  bool read_fpga_temperature(TermOutput& termout, int format);

  bool read_fpga_scrap(TermOutput& termout, int format);
  bool write_fpga_scrap(const char *data);

  bool read_fpga_weights(TermOutput& termout, int format);
  bool write_fpga_weights(const char *data);

  bool read_sst_offload_weighted_subbands(TermOutput& termout, int format);
  bool write_sst_offload_weighted_subbands(const char *data);
  bool read_sst_offload_enable(TermOutput& termout, int format);
  bool write_sst_offload_enable(const char *data);
  bool read_sst_offload_hdr_eth_destination_mac(TermOutput& termout, int format);
  bool write_sst_offload_hdr_eth_destination_mac(const char *data);
  bool read_sst_offload_hdr_ip_destination_address(TermOutput& termout, int format);
  bool write_sst_offload_hdr_ip_destination_address(const char *data);
  bool read_sst_offload_hdr_udp_destination_port(TermOutput& termout, int format);
  bool write_sst_offload_hdr_udp_destination_port(const char *data);

  bool read_bst_offload_enable(TermOutput& termout, int format);
  bool write_bst_offload_enable(const char *data);
  bool read_bst_offload_hdr_eth_destination_mac(TermOutput& termout, int format);
  bool write_bst_offload_hdr_eth_destination_mac(const char *data);
  bool read_bst_offload_hdr_ip_destination_address(TermOutput& termout, int format);
  bool write_bst_offload_hdr_ip_destination_address(const char *data);
  bool read_bst_offload_hdr_udp_destination_port(TermOutput& termout, int format);
  bool write_bst_offload_hdr_udp_destination_port(const char *data);
  bool read_bst_offload_nof_beamlets_per_packet(TermOutput& termout, int format);
  bool write_bst_offload_nof_beamlets_per_packet(const char *data);

  bool read_xst_subband_select(TermOutput& termout, int format);
  bool write_xst_subband_select(const char *data);
  bool read_xst_integration_interval(TermOutput& termout, int format);
  bool write_xst_integration_interval(const char *data);
  bool read_xst_processing_enable(TermOutput& termout, int format);
  bool write_xst_processing_enable(const char *data);
  bool read_xst_offload_enable(TermOutput& termout, int format);
  bool write_xst_offload_enable(const char *data);
  bool read_xst_offload_hdr_eth_destination_mac(TermOutput& termout, int format);
  bool write_xst_offload_hdr_eth_destination_mac(const char *data);
  bool read_xst_offload_hdr_ip_destination_address(TermOutput& termout, int format);
  bool write_xst_offload_hdr_ip_destination_address(const char *data);
  bool read_xst_offload_hdr_udp_destination_port(TermOutput& termout, int format);
  bool write_xst_offload_hdr_udp_destination_port(const char *data);

  bool read_processing_enable(TermOutput& termout, int format);
  bool write_processing_enable(const char *data);

  bool read_sdp_info_station_id(TermOutput& termout, int format);
  bool write_sdp_info_station_id(const char *data);
  bool read_sdp_info_observation_id(TermOutput& termout, int format);
  bool write_sdp_info_observation_id(const char *data);
  bool read_sdp_info_nyquist_sampling_zone_index(TermOutput& termout, int format);
  bool write_sdp_info_nyquist_sampling_zone_index(const char *data);
  bool read_sdp_info_antenna_band_index(TermOutput& termout, int format);
  bool read_sdp_info_f_adc(TermOutput& termout, int format);
  bool read_sdp_info_fsub_type(TermOutput& termout, int format);
  bool read_wg_enable(TermOutput& termout, int format);

  bool write_wg_enable(const char *data);
  bool read_wg_amplitude(TermOutput& termout, int format);
  bool write_wg_amplitude(const char *data);
  bool read_wg_phase(TermOutput& termout, int format);
  bool write_wg_phase (const char *data);
  bool write_wg_frequency(const char *data);
  bool read_wg_frequency(TermOutput& termout, int format);

  bool read_bsn_monitor_input_sync_timeout(TermOutput& termout, int format, int mode);
  bool read_bsn_monitor_input_bsn(TermOutput& termout, int format, int mode);
  bool read_bsn_monitor_input_nof_packets(TermOutput& termout, int format, int mode);
  bool read_bsn_monitor_input_nof_valid(TermOutput& termout, int format, int mode);
  bool read_bsn_monitor_input_nof_err(TermOutput& termout, int format, int mode);

  bool read_jesd204b_csr_rbd_count(TermOutput& termout, int format, int mode);
  bool read_jesd204b_csr_dev_syncn(TermOutput& termout, int format, int mode);
  bool read_jesd204b_rx_err0(TermOutput& termout, int format, int mode);
  bool read_jesd204b_rx_err1(TermOutput& termout, int format, int mode);

  bool write_signal_input_samples_delay(const char *data);
  bool read_signal_input_samples_delay(TermOutput& termout, int format);
  bool read_signal_input_mean(TermOutput& termout, int format, int mode);
  bool read_signal_input_rms(TermOutput& termout, int format, int mode);

  bool write_subband_weights(const char *data);
  bool read_subband_weights(TermOutput& termout, int format);

  bool read_sdp_info_block_period(TermOutput& termout, int format);

  bool read_time_since_last_pps(TermOutput& termout, int format, int mode);
  bool write_wdi_override(TermOutput& termout);

  CMMap read_reg_map();

public:
  Periph_fpga(uint global_nr, std::string ipaddr, uint n_beamsets);
  ~Periph_fpga();


  bool read(TermOutput& termout, const std::string addr, const std::string type,
            char *data, const uint nvalues, const int format);
  bool write(TermOutput& termout, const std::string addr, const std::string type,
             char *data, const uint nvalues, const int format);
  bool monitor(TermOutput& termout);

  CMMap* getMMap(void) { return mmap; };
  bool isOnline(void) { return Online; }
  bool isMasked(void) { return Masked; }
  void setMasked(bool mask) { Masked = mask; }
  uint32_t ppsOffsetCnt(void) { return my_pps_offset_cnt; }
};

#endif // __PERIPH_FPGA_H__
