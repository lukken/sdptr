/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . register class used to hold register information
* *********************************************************************** */

#include <iostream>
#include <iomanip>
#include <sstream>

#include "registers.h"

using namespace std;


int32_t reg_format_size_in_bytes(int32_t type) {
  int32_t sz = 0;
  switch (type) {
      case REG_FORMAT_FLOAT: {
          sz = sizeof(float);
      } break;
      case REG_FORMAT_DOUBLE: {
          sz = sizeof(double);
      } break;
      case REG_FORMAT_INT64: {
          sz = sizeof(int64_t);
      } break;
      case REG_FORMAT_UINT64: {
          sz = sizeof(uint64_t);
      } break;
      case REG_FORMAT_INT32: {
          sz = sizeof(int32_t);
      } break;
      case REG_FORMAT_UINT32: {
          sz = sizeof(uint32_t);
      } break;
      case REG_FORMAT_INT16: {
          sz = sizeof(int16_t);
      } break;
      case REG_FORMAT_UINT16: {
          sz = sizeof(uint16_t);
      } break;
      case REG_FORMAT_INT8: {
          sz = sizeof(int8_t);
      } break;
      case REG_FORMAT_UINT8: {
          sz = sizeof(uint8_t);
      } break;
      case REG_FORMAT_STRING: {
          sz = sizeof(char) * SIZE1STRING;
      } break;
      case REG_FORMAT_BOOLEAN: {
          sz = sizeof(bool);
      } break;
      default: {
          cout << "size_in_bytes, Not supported datatype (" << type << ")" << endl;
      } break;
  }
  return sz;
}

int32_t reg_format_size_in_words(int32_t type) {
    int32_t sz = reg_format_size_in_bytes(type);
    if (sz == 0) return 0;
    if (sz <= 4) return 1;
    return (int32_t)(sz / 4);
}

//
// CMMap used for holding fpga mm register information
//
CMMap::CMMap() {}
CMMap::~CMMap() { reg.clear(); }

bool CMMap::add_register(const string name, const uint32_t base, const uint32_t n_mm_ports,
                         const uint32_t span, const uint32_t mask,
                         const uint32_t shift, const string access,
                         const string type, const uint32_t peripheral_span, const uint32_t mm_port_span)
{
    // cout << "CMMap::add_register: " << name << endl;
    if (find_register(name)) {
        cerr << "CMMap::add_register: " << name << " already exist!" << endl;
        return false;
    }
    register_info r = {base, n_mm_ports, span, mask, shift, access, type, mm_port_span, peripheral_span};
    reg.insert(reg.end(), pair<string, register_info>(name, r));
    return true;
}

bool CMMap::update_register(const string name, const uint32_t base, const uint32_t n_mm_ports,
                            const uint32_t span, const uint32_t mask,
                            const uint32_t shift, const string access,
                            const string type, const uint32_t peripheral_span, const uint32_t mm_port_span)
{
    // cout << "CMMap::update_register: " << name << endl;
    if (!find_register(name)) {
        cerr << "CMMap::update_register: " << name << " not exist!" << endl;
        return false;
    }
    reg[name].type = type;
    reg[name].access = access;
    reg[name].shift = shift;
    reg[name].mask = mask;
    reg[name].span = span;
    reg[name].base = base;
    reg[name].n_mm_ports = n_mm_ports;
    reg[name].mm_port_span = mm_port_span;
    reg[name].peripheral_span = peripheral_span;
    return true;
}

bool CMMap::find_register(string name)
{
    bool ret=false;
    auto i = reg.find(name);
    if (i != reg.end()) {
        ret = true;
    }
    return ret;
}

// #define MAX_NOF_INSTANCES 1024

void CMMap::print(ostringstream& strs, string prefix)
{
    for (auto m : reg) {
        strs << prefix << " register[" << m.first << "]"
             << " base=0x"   << hex << m.second.base
             << " size="     << dec << m.second.span
             << " mask=0x"   << hex << m.second.mask
             << " shift="    << dec << m.second.shift
             << " access="   << m.second.access
             << " type="     << m.second.type
             << endl;
    }
}

void CMMap::print_screen(void)
{
    for (auto m : reg) {
        cout << " register[" << m.first << "]"
                  << ", base=0x"          << hex << m.second.base
                  << ", n_ports="         << dec << m.second.n_mm_ports
                  << ", size="            << dec << m.second.span
                  << ", mask=0x"          << hex << m.second.mask
                  << ", shift="           << dec << m.second.shift
                  << ", access="          << m.second.access
                  << ", type="            << m.second.type
                  << ", mm_port_span="    << m.second.mm_port_span
                  << ", peripheral_span=" << m.second.peripheral_span
                  << endl;
    }
}


vector<string> CMMap::getRegnames(string prefix)
{
    vector<string> regnames;
    for (auto m : reg) {
        regnames.push_back(prefix + m.first);
    }
    return regnames;
}

vector<string> CMMap::getRegnames_full(string prefix)
{
    ostringstream strs;

    vector<string> regnames;
    for (auto m : reg) {
        strs.str("");
        strs.clear();
        strs << m.second.access << " "
             << setw(4) << m.second.type << " "
             << "0x" << setfill('0') << hex << setw(8) << m.second.base << " "
             <<         setfill(' ') << dec << setw(4) << m.second.span << " "
             << prefix << m.first;

        regnames.push_back(strs.str());
    }
    return regnames;
}

uint32_t CMMap::getAddr(const string name)
{
    if (!find_register(name)) {
        throw runtime_error("Register[" + name + "] not existing!");
    }
    uint32_t addr = getBaseAddr(name) * sizeof(uint32_t);
    return addr;
}

uint32_t CMMap::getValidAddr(const string name,
                                   const uint32_t size)
{
    if (!find_register(name)) {
        throw runtime_error("Register[" + name + "] not existing!");
    }
    uint32_t base = getBaseAddr(name) * sizeof(uint32_t);
    uint32_t span = getSpan(name);

    if (size > span && !type_isfifo(name)) {
        throw runtime_error("Register[" + name + "] getValidAddr() out of range!");
    }
    uint32_t addr = base;
    return addr;
}

bool CMMap::getReadPermission(const string name)
{
    string perm = getPerm(name);
    if (perm == "RO" || perm == "RW") {
        return true;
    }
    else {
        throw runtime_error("Register[" + name + "] getReadPermission() fail!");
    }
    return false;
}

bool CMMap::getWritePermission(const string name)
{
    string perm = getPerm(name);
    if (perm == "WO" || perm == "RW") {
        return true;
    }
    else {
        throw runtime_error("Register[" + name + "] getWritePermission() fail!");
    }
    return false;
}

void CMMap::setAllPermission_NA(void)
{
    cerr << "CMMap::setAllPermission_NA()" << endl;
    for (auto &m : reg) {
        m.second.access = "NA";
    }
}

//
// CPointMap used for assigning opc-ua points
//
CPointMap::CPointMap() {}

CPointMap::~CPointMap() { reg.clear(); }

bool CPointMap::add_register(const string name, const string relname, const uint32_t n_nodes, const uint32_t n_data,
                             const string access, const uint32_t format)
{
    cout << "CPointMap::add_register: " << name << endl;
    if (find_register(name)) {
        cerr << "CPointMap::add_register: " << name << " already exist!" << endl;
        return false;
    }
    register_info r={relname, n_nodes, n_data, access, format};
    reg.insert(reg.end(), pair<string, register_info>(name, r));
    return true;
}

bool CPointMap::update_register(const string name, const string relname, const uint32_t n_nodes, const uint32_t n_data,
                                const string access, const uint32_t format)
{
    cout << "CPointMap::update_register: " << name << endl;
    if (!find_register(name)) {
        cerr << "CPointMap::update_register: " << name << " not exist!" << endl;
        return false;
    }
    reg[name].relname = relname;
    reg[name].n_nodes = n_nodes;
    reg[name].n_data = n_data;
    reg[name].access = access;
    reg[name].format = format;
    return true;
}

bool CPointMap::find_register(string name)
{
    bool ret=false;
    auto i = reg.find(name);
    if (i != reg.end()) {
        ret = true;
    }
    return ret;
}

void CPointMap::print(ostringstream& strs, string prefix)
{
    for (auto m : reg) {
        strs << prefix << " register[" << m.first << "]"
             << " relname="  << m.second.relname
             << " n_nodes="  << dec << m.second.n_nodes
             << " n_data="   << dec << m.second.n_data
             << " access="   << m.second.access
             << " format="   << m.second.format << endl;
    }
}

void CPointMap::print_screen(void)
{
    for (auto m : reg) {
        cout << " register[" << m.first << "]"
                  << " relname="  << m.second.relname
                  << " n_nodes="  << dec << m.second.n_nodes
                  << " n_data="   << dec << m.second.n_data
                  << " access="   << m.second.access
                  << " format="   << m.second.format << endl;
    }
}

vector<string> CPointMap::getRegnames(string prefix)
{
    vector<string> regnames;
    for (auto m : reg) {
        regnames.push_back(prefix + m.first);
    }
    return regnames;
}

bool CPointMap::getReadPermission(const string name)
{
    string perm = getPerm(name);
    if (perm == "RO" || perm == "RW") {
        return true;
    }
    else {
        throw runtime_error("Register[" + name + "] getReadPermission() fail!");
    }
    return false;
}

bool CPointMap::getWritePermission(const string name)
{
    string perm = getPerm(name);
    if (perm == "WO" || perm == "RW") {
        return true;
    }
    else {
        throw runtime_error("Register[" + name + "] getWritePermission() fail!");
    }
    return false;
}

void CPointMap::setAllPermission_NA(void)
{
    cerr << "CPointMap::setAllPermission_NA()" << endl;
    for (auto &m : reg) {
        m.second.access = "NA";
    }
}