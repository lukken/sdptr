/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . board info
* *********************************************************************** */

#ifndef _REENTRANT
#error ACK! You need to compile with _REENTRANT defined since this uses threads
#endif

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <arpa/inet.h> //inet_pton

#include "board.h"
#include "tools/parse.h"

using namespace std;

extern int debug;

Board::Board(list<class Node*>& nodelist)
{
    NODE = nodelist;
}

Board::~Board()
{
    for (auto node : NODE) {
        delete node;
    }
}

Node * Board::select_node(const int nr)
{
    for (auto node : NODE) {
        if (node->GetGlobalNr() == (uint)nr) {
            return node;
        }
    }
    throw runtime_error("select_node: not found");
}

uint Board::node_number(Node *node)
{
    return (node->GetUniboardNr() * 4) + node->GetLocalNr();
}

vector<int> Board::get_nodes(void)
{
    vector<int> nodes;
    for (auto node : NODE) {
        nodes.push_back(node->GetGlobalNr());
    }
    return nodes;
}

bool Board::monitor(TermOutput& termout)
{
    // cout << "board_monitor start" << endl;
    bool retval=false;
    uint retcnt=0;
    vector<int> nodes = get_nodes();
    for (uint idx=0; idx<nodes.size(); idx++) {
        auto node = select_node(nodes[idx]);
        if (idx > 0) {
        }
        try {
            if (node->monitor(termout)) {
                retcnt++;
            }
        } catch (exception& e) {
            cerr << e.what() << endl;
        }
    }
    retval = (retcnt == nodes.size());
    return retval;
}

uint Board::ipaddr_to_id(const string ipaddr)
{
    struct in_addr sa;
    // store this IP address in sa:
    inet_pton(AF_INET, ipaddr.c_str(), &(sa.s_addr));

    uint32_t ip = ntohl(sa.s_addr);
    // example: ipaddr="10.99.0.2" then ip will be: 0xa630002

    /*
     * Assuming every uniboard has 8 nodes and the ipaddress of the node is held
     * in the least significand IP address tuble.
     */
    uint8_t node = (uint8_t)(ip & 0xf) - 1;
    uint8_t unb = (uint8_t)(ip >> 8); // unb number is stored in 2nd least significant tuble
    return (unb * 8 + node);
}
