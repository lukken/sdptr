/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . board info
* *********************************************************************** */

#ifndef BOARD_H
#define BOARD_H 1

#include <string>
#include <list>
#include <vector>
#include <sstream>

#include "node.h"
#include "tools/util.h"
#include "registers.h"


#define c_NOF_PN_on_UNB 4

class Board
{
private:
    Tictoc tictoc;

    std::list<class Node*> NODE;

    Node * select_node(const int nodeId);
    uint node_number(Node *node);

public:
    Board(std::list<class Node*>& nodelist);
    ~Board();

    std::vector<int> get_nodes(void);
    bool monitor(TermOutput& termout);

    uint ipaddr_to_id(const std::string ipaddr);
};
#endif

