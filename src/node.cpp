/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . This is the class that does the communication with the nodes, using several
#   threads with workers.
* *********************************************************************** */

#ifndef _REENTRANT
#error ACK! You need to compile with _REENTRANT defined since this uses threads
#endif

#include <unistd.h>
#include <stdexcept>
#include <cstring>
#include <cstdio>

#include "tools/util.h"
#include "node.h"

using namespace std;

extern int debug;
volatile bool WorkersRunning = true;

cCmd::cCmd() { init(); }
void cCmd::init()
{
    cmdId = ' ';
    memset(relative_addr, 0, sizeof(relative_addr));
    memset(type, 0, sizeof(type));
    nvalues = 0;
    format = REG_FORMAT_UNKNOWN;
    memset(data, 0, sizeof(data));
}
cCmd::~cCmd() {}

cReply::cReply() { init(); }
void cReply::init()
{
    cmdId = ' ';
    retval = 0;
    memset(data, 0, sizeof(data));
    nvalues = 0;
    format = REG_FORMAT_UNKNOWN;
}
cReply::~cReply() {}


void Node::worker()
{
    TermOutput termout;

    cout << "Node::worker thread for client"
         << " Uniboard=" << UniboardNr
         << " New node IP=" << myIPaddr
         << " type=" << Type
         << " localnr=" << LocalNr
         << " globalnr=" << GlobalNr << endl;

    while (WorkersRunning) {
        cCmd p;
        if (cmd_queue.Pop(p) == false) {
            continue;
        }
        // cout << " p.relative_addr=" << p->relative_addr << endl;
        // cout << " p.type=" << p->type << endl;
        // cout << " p.nvalues=" << p->nvalues << endl;
        // cout << " p.format=" << p->format << endl;
        // cout << " p.cmd=" << p->cmdId << endl;

        termout.clear();
        cReply r;
        r.cmdId = p.cmdId;
        try {
            switch (p.cmdId) {
                case 'S': {
                    printf("Node::worker job for Uniboard=%d New node IP=%s shutdown!!\n", UniboardNr, myIPaddr.c_str());
                    WorkersRunning = false;
                    r.format = p.format;
                    continue; // shutdown worker thread
                } break;
                case 'M': {
                    r.retval = periph_fpga->monitor(termout);
                    r.format = p.format;
                    continue; // monitor points
                } break;
                case 'R': {  // read
                    if (strcmp(p.type, "fpga") == 0 || strcmp(p.type, "mm") == 0) {
                        r.retval = periph_fpga->read(termout, p.relative_addr, p.type, p.data, p.nvalues, p.format);
                        r.nvalues = termout.nof_vals;
                        r.format = termout.datatype;
                    }
                } break;
                case 'W': {  // write
                    if (strcmp(p.type, "fpga") == 0 || strcmp(p.type, "mm") == 0) {
                        r.retval = periph_fpga->write(termout, p.relative_addr, p.type, p.data, p.nvalues, p.format);
                        r.nvalues = p.nvalues;
                        r.format = p.format;
                    }
                } break;
                default: {
                    throw runtime_error("Node::nothing to do");
                } break;
            }

        } catch (exception& e) {
            r.retval = 0;
            cerr << e.what();
            //cerr << "Node::worker exception: " << e.what() << endl;
        } catch (...) {
            r.retval = 0;
            //cerr << "Node::worker '...' exception" << endl;
        }
        // cout << " p.relative_addr=" << p->relative_addr << endl;
        // cout << " p.type=" << p->type << endl;
        // cout << " r.nvalues=" << r.nvalues << endl;
        // cout << " r.format=" << r.format << endl;
        // cout << " p.cmd=" << p->cmdId << endl;

        int sz = reg_format_size_in_bytes(r.format);
        memcpy((void *)r.data, (void *)termout.val, termout.nof_vals * sz);

        reply_queue.Push(r);
    }
    // cout << "Node::worker job"
    //      << " Uniboard=" << UniboardNr
    //      << " New node IP=" << myIPaddr
    //      << " leaving!!" << endl;
}

Node::Node(const string ipaddr, const uint unb, const uint localnr, const string type, const uint nof_beamsets):
    myIPaddr(ipaddr),
    UniboardNr(unb),
    LocalNr(localnr),
    GlobalNr(localnr + FPGAS_PER_BOARD * unb),
    n_beamsets(nof_beamsets)
{
    periph_fpga = new Periph_fpga(GlobalNr, ipaddr, n_beamsets);

    if (type != "pn") {
        throw runtime_error("invalid node type: \"" + type + "\"");
    }

    Type = type;

    //syslog(LOG_INFO,"Uniboard=%d New node IP=%s  type=%s localnr=%d globalnr=%d\n",
    //       UniboardNr, myIPaddr.c_str(), Type.c_str(), LocalNr, GlobalNr);
    cout << "Uniboard=" << UniboardNr
         << " New node IP=" << myIPaddr
         << " type=" << Type
         << " localnr=" << LocalNr
         << " globalnr=" << GlobalNr
         << " enabled" << endl;

    WorkersRunning = true;
    worker_thread = new thread(&Node::worker, this);
}

Node::~Node()
{
    WorkersRunning = false;

    cCmd p;
    p.cmdId = 'S';
    cmd_queue.Push(p);

    cout << "Joining threads" << endl;
    worker_thread->join();
    cout << "Deleting threads" << endl;
    if (worker_thread != NULL) {
        delete worker_thread;
    }
    if (periph_fpga != NULL) {
        delete periph_fpga;
    }
}

bool Node::monitor(TermOutput& termout) {

    cCmd p;
    p.cmdId = 'M';
    cmd_queue.Push(p);
    return true;
}

CMMap * Node::get_MMap(void)
{
    return periph_fpga->getMMap();
}

bool Node::isOnline(void)
{
    return periph_fpga->isOnline();
}

bool Node::isMasked()
{
    return periph_fpga->isMasked();
}

void Node::setMasked(bool mask)
{
    periph_fpga->setMasked(mask);
}

uint32_t Node::ppsOffsetCnt(void) {
    return periph_fpga->ppsOffsetCnt();
}


bool Node::exec_cmd(const char cmd, const string relative_addr,
                    const string type, const char *data,
                    const int nvalues, const int format)
{
    cCmd p;
    p.cmdId = cmd;
    strcpy(p.relative_addr, relative_addr.c_str());
    strcpy(p.type, type.c_str());
    p.nvalues = nvalues;
    p.format = format;
    if (cmd == 'W') {
        memcpy((void *)p.data, (void *)data, nvalues*reg_format_size_in_bytes(format));
        // cout << " p.relative_addr=" << p.relative_addr 
        //      << " p.nvalues=" << p.nvalues
        //      << " p.format=" << p.format << endl;
    }

    cmd_queue.Push(p);
    return true;
}

bool Node::exec_reply(TermOutput& termout)
{
    cReply r;
    if (reply_queue.Pop(r) == false) {
        return false;
    }

    termout.nof_vals = r.nvalues;
    termout.datatype = r.format;
    // cout << "received " << r->nof_vals << " bytes from worker node=" << GlobalNr 
    //      << " retval=" << r->retval << " datatype=" << r->datatype << endl;

    if (r.retval) {
        int sz = reg_format_size_in_bytes(r.format);
        memcpy((void *)termout.val, (void *)r.data, r.nvalues * sz);
    }
    return r.retval;
}

