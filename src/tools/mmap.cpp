/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */

#include <string>
#include <iostream>

#include "mmap.h"

using namespace std;


typedef struct {
    string   port_name;
    uint32_t n_peripherals;
    uint32_t n_ports;
    string   port_type;
    string   field_name;
    uint32_t base_addr;  // in MM words
    uint32_t n_fields;
    string   acces_mode;
    string   radix;
    uint32_t mm_mask;
    uint32_t user_mask;
    uint32_t span;
    uint32_t shift;
    uint32_t peripheral_span;
    uint32_t mm_port_span;
} mm_info_t;

void mmap_add_register(CMMap& regmap, mm_info_t mm_info)
{
    bool update;
    uint32_t base_addr = mm_info.base_addr;
    uint32_t addr;
    string full_name;
    for (uint i=0; i<mm_info.n_peripherals; i++) {
        for (uint j=0; j<mm_info.n_ports; j++) {
            update = false;
            full_name = "mm/" + to_string(i) + "/" + mm_info.port_name + "/" + to_string(j) + "/" + mm_info.field_name;

            if (regmap.find_register(full_name)) {
                // mm_info.base_addr = regmap.getBaseAddr(full_name);
                update = true;
            }
            addr = base_addr + (i * mm_info.peripheral_span) + (j * mm_info.mm_port_span);

            if (update) {
                regmap.update_register(full_name,
                                       addr,
                                       mm_info.n_ports,
                                       mm_info.span,
                                       mm_info.mm_mask,
                                       mm_info.shift,
                                       mm_info.acces_mode,
                                       mm_info.port_type,
                                       mm_info.peripheral_span,
                                       mm_info.mm_port_span);
            }
            else {
                regmap.add_register(full_name,
                                    addr,
                                    mm_info.n_ports,
                                    mm_info.span,
                                    mm_info.mm_mask,
                                    mm_info.shift,
                                    mm_info.acces_mode,
                                    mm_info.port_type,
                                    mm_info.peripheral_span,
                                    mm_info.mm_port_span);
            }
        }
    }
}

CMMap mmap_to_regmap(istringstream& iss)
{
    CMMap regmap;
    ostringstream err_str;

    mm_info_t mm_info;
    mm_info_t last_mm_info;

    uint64_t start_addr44;  // 64bit value, but first 44 bits are used for address.

    int mm_mask_hi, mm_mask_lo;
    int user_mask_hi, user_mask_lo;

    uint32_t mask;
    char line[250];
    string val_str;
    bool same_mask;
    bool same_field_name;
    // char sep;

    iss.getline(line, sizeof(line));
    iss.getline(line, sizeof(line));
    while (iss.getline(line, sizeof(line))) {
        same_mask = false;
        same_field_name = false;
        mm_info = last_mm_info;
        stringstream strs(line);

        strs >> val_str;  // get port name
        if (val_str != "-") {
            mm_info.port_name = val_str;
            mm_info.peripheral_span = 1;
            mm_info.mm_port_span = 1;
            mm_info.n_peripherals = 1;
            mm_info.n_ports = 1;
        }

        strs >> val_str;  // get n_peripherals
        // mm_info.n_peripherals = 1;
        if (val_str != "-") {
            mm_info.n_peripherals = stoi(val_str);
        }

        strs >> val_str;  // get n_ports
        // mm_info.n_ports = 1;
        if (val_str != "-") {
            mm_info.n_ports = stoi(val_str);
        }

        strs >> val_str;  // get port_type
        if (val_str != "-") {
            mm_info.port_type = val_str;
        }

        strs >> val_str;  // get field_name
        if (val_str != "-") {
            mm_info.field_name = val_str;
            mm_info.span = 0;
        }
        else {
            same_mask = true;
            same_field_name = true;
        }

        strs >> hex >> start_addr44; // get start_address in dword addresses
        if (!same_field_name) {
            mm_info.base_addr = (uint32_t)start_addr44;
        }

        strs >> val_str;  // get n_fields
        if (val_str != "-") {
            mm_info.n_fields = stoi(val_str);
        }

        strs >> val_str;  // get acces_mode
        if (val_str != "-") {
            mm_info.acces_mode = val_str;
        }

        strs >> val_str;  // get radix
        if (val_str != "-") {
            mm_info.radix = val_str;
        }

        /* Code to remove sscanf */
        // strs >> mm_mask_hi;
        // strs >> sep;
        // strs >> mm_mask_lo;
        strs >> val_str;  // get mm_mask hi/lo
        sscanf(val_str.c_str(), "%d:%d", &mm_mask_hi, &mm_mask_lo);
        mask = 0;
        for (int i=mm_mask_lo; i<=mm_mask_hi; i++) {
            mask |= (1<<i);
        }
        if (!same_mask) {
            mm_info.mm_mask = mask;
        }
        mm_info.shift = mm_mask_lo;
        
        /* Code to remove sscanf */ 
        // if (strs.peek() != "-") {
        //     sep = strs.get()
        //     mm_info.user_mask = mm_info.mm_mask;
        //     mm_info.span = mm_info.n_fields;
        // }
        // else {
        //     strs >> user_mask_hi;
        //     strs >> sep;
        //     strs >> user_mask_lo;
        //     mask = 0;
        //     for (int i=mm_mask_lo; i<=mm_mask_hi; i++) {
        //         mask |= (1<<i);
        //     }
        //     mm_info.user_mask = mask;
        //     mm_info.span = (uint)(((user_mask_hi - user_mask_lo + 1) * mm_info.n_fields) / (mm_mask_hi - mm_mask_lo + 1));
        // }
        strs >> val_str;  // get user_mask hi/lo
        uint32_t span;
        if (val_str != "-") {
            sscanf(val_str.c_str(), "%d:%d", &user_mask_hi, &user_mask_lo);
            mask = 0;
            for (int i=mm_mask_lo; i<=mm_mask_hi; i++) {
                mask |= (1<<i);
            }
            mm_info.user_mask = mask;
            span = (uint)(((user_mask_hi - user_mask_lo + 1) * mm_info.n_fields) / (mm_mask_hi - mm_mask_lo + 1));
        }
        else {
            mm_info.user_mask = mm_info.mm_mask;
            span = mm_info.n_fields;
        }
        mm_info.span += span;


        strs >> val_str;  // get peripheral_span
        if (val_str != "-") {
            if (mm_info.n_peripherals > 1 || mm_info.n_ports > 1) {
                mm_info.peripheral_span = stoi(val_str);
            }
        }

        strs >> val_str;  // get mm_port_span
        if (val_str != "-") {
            if (mm_info.n_peripherals > 1 || mm_info.n_ports > 1) {
                mm_info.mm_port_span = stoi(val_str);
            }
        }

        if (strs.fail() || strs.bad()) {
            cerr << "import_mmap_file: invalid" << endl;
            cout << "import_mmap_file: invalid" << endl;
        }
        else {
            last_mm_info = mm_info;
            mmap_add_register(regmap, mm_info);
        }
    }

    // cout << "regmap:" << endl;
    // regmap.print_screen();

    return regmap;
}
