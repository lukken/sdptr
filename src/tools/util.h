/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . 
* *********************************************************************** */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <string>
#include <vector>
#include <ctime>
#include <sstream>
#include <chrono>
#include <cstring>

#include "../registers.h"

class Tictoc {
private:
  std::string name;
  std::clock_t c_start;
  std::chrono::high_resolution_clock::time_point t_start;
public:
  Tictoc() {};
  ~Tictoc() {};
  void tic(std::string idstr="");
  void toc(void);
};

class TermOutput {
public:

#define SIZE400K    400000


  char val[SIZE400K];
  unsigned int nof_vals;
  int datatype;

  TermOutput() {}
  ~TermOutput() {};

  void clear(void) {
      nof_vals = 0;
      datatype = 0;
      std::memset((void *)val, 0, SIZE400K);
  }
};


unsigned int next_power_of_2(unsigned int v);
unsigned int ceil_log2(unsigned int val);
unsigned int ceil_div(unsigned int num, unsigned int den);
std::vector<uint> unique_and_sort(std::vector<uint> v);
void probe(std::string s);
uint select_get_msb_downto_lsb(uint input, uint bit_msb, uint bit_lsb);
uint select_set_msb_downto_lsb(uint input, uint bit_msb, uint bit_lsb);
std::vector<std::vector<uint>> transpose(std::vector<std::vector<uint>> Rin);
bool find_in_vector(std::vector<uint>& V, uint p);

#endif // __UTIL_H__
