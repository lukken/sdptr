/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . parsers
* *********************************************************************** */

#include <stdexcept>
#include <iostream>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "parse.h"

using namespace std;
namespace ublas = boost::numeric::ublas;

extern int debug;

int parse_int(istringstream& vecstr)
{
    int num;

    vecstr >> num;
    if (debug) {
        cout << "parse_int:" << num << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("Invalid string");
    }
    return num;
}

int parse_uint_nothrow(istringstream& vecstr)
{
    int num;

    vecstr >> num;
    if (debug) {
        cout << "parse_int_nothrow:" << num << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        return -1;
    }
    return num;
}

double parse_double(istringstream& vecstr) 
{
    double num;

    vecstr >> num;
    if (debug) {
        cout << "parse_double:" << num << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("Invalid string");
    }
    return num;
}

double parse_double_nothrow(istringstream& vecstr)
{
    double num;

    vecstr >> num;
    if (debug) {
        cout << "parse_double_nothrow:" << num << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        return NAN;
    }
    return num;
}

complex<double> parse_complex_double(istringstream& vecstr) 
{
    complex<double> num;
    vecstr >> num;
    if (debug) {
        cout << "parse_complex_double:" << num << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("Invalid string");
    }
    return num;
}

// vectors:
vector<int> parse_int_vector(istringstream& vecstr) 
{
    vector<int> v;
    int num;
    char c;

    vecstr >> c;
    if (c != '[') {
        throw runtime_error("Invalid string [ bracket");
    }
    while (!vecstr.eof()) {

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            if (debug) {
                cout << "parse_int_vector: empty vector" << endl;
            }
            vecstr >> c; // flush it from istringstream
            return v;
        }
        vecstr >> num;

        if (vecstr.fail() || vecstr.bad()) {
            throw runtime_error("Invalid string");
        }
        v.push_back(num);

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            break;
        }
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("parse_int_vector error");
    }
    if (debug) {
        cout << "parse_int_vector result: ";
        for (int i=0; i<(int)v.size(); i++) {
            cout << v[i] << " ";
        }
        cout << endl;
    }
    return v;
}

vector<int> parse_int_vector_range(istringstream& vecstr) 
{
    vector<int> v;
    int num;
    char c;

    vecstr >> c;
    if (c != '[') {
        throw runtime_error("Invalid string [ bracket");
    }
    while (!vecstr.eof()) {
        c = vecstr.peek();
        if (c == ']' || c == 0) {
            if (debug) {
                cout << "parse_int_vector_range: empty vector" << endl;
            }
            vecstr >> c; // flush it from istringstream
            return v;
        }

        vecstr >> num;
        if (vecstr.fail() || vecstr.bad()) {
            throw runtime_error("Invalid number in string: " + to_string(num));
        }

        c = vecstr.peek();
        if (c == ':') {
            vecstr >> c; // flush it from istringstream
            int endnum, startnum = num;

            vecstr >> endnum;
            if (vecstr.fail() || vecstr.bad()) {
                throw runtime_error("Invalid string");
            }
            for (int i=startnum; i<=endnum; i++) {
                v.push_back(i);
            }
        } 
        else {
            v.push_back(num);
        }

        c = vecstr.peek();
        if (c == ',') {
            vecstr >> c; // flush it from istringstream
        }
        else if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            break;
        }
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("parse_int_vector_range error");
    }
    if (debug) {
        cout << "parse_int_vector_range result: ";
        for (int i=0; i<(int)v.size(); i++) {
            cout << v[i] << " ";
        }
        cout << endl;
    }
    return v;
}

vector<uint> parse_uint_vector_range(istringstream& vecstr) 
{
    vector<int> vint = parse_int_vector_range(vecstr);
    vector<uint> vuint;
    for (auto v : vint) {
        vuint.push_back((uint)v);
    }
    return vuint;
}

vector<int64_t> parse_int64_vector_range(istringstream& vecstr) 
{
    vector<int64_t> v;
    int64_t num;
    char c;

    vecstr >> c;
    if (c != '[') throw runtime_error("Invalid string [ bracket");
    while (!vecstr.eof()) {

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            if (debug) {
                cout << "parse_int64_vector_range: empty vector" << endl;
            }
            vecstr >> c; // flush it from istringstream
            return v;
        }

        vecstr >> num;
        if (vecstr.fail() || vecstr.bad()) {
            throw runtime_error("Invalid number in string: " + to_string(num));
        }

        c = vecstr.peek();
        if (c == ':') {
            vecstr >> c; // flush it from istringstream
            int endnum, startnum = num;

            vecstr >> endnum;
            if (vecstr.fail() || vecstr.bad()) {
                throw runtime_error("Invalid string");
            }
            for (int i=startnum; i<=endnum; i++) {
                v.push_back(i);
            }
        } 
        else {
            v.push_back(num);
        }

        c = vecstr.peek();
        if (c == ',') {
            vecstr >> c; // flush it from istringstream
        }
        else if (c==']' || c==0) {
            vecstr >> c; // flush it from istringstream
            break;
        }
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("parse_int64_vector_range error");
    }
    if (debug) {
        cout << "parse_int64_vector_range result: ";
        for (int i=0; i<(int)v.size(); i++) {
            cout << v[i] << " ";
        }
        cout << endl;
    }
    return v;
}

vector<double> parse_double_vector(istringstream& vecstr) 
{
    vector<double> v;
    double num;
    char c;

    vecstr >> c;
    if (c != '[') throw runtime_error("Invalid string [ bracket");
    while (!vecstr.eof()) {

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            return v;
        }

        vecstr >> num;

        if (vecstr.fail() || vecstr.bad()) {
            throw runtime_error("Invalid string");
        }
        v.push_back(num);

        c = vecstr.peek();
        if (c == ',') {
            vecstr >> c; // flush it from istringstream
        }
        if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            break;
        }
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("parse_double_vector error");
    }
    if (debug) {
        cout << "parse_double_vector result: ";
        for (int i=0; i<(int)v.size(); i++) {
            cout << v[i] << " ";
        }
        cout << endl;
    }
    return v;
}

vector< complex<double> > parse_complex_vector(istringstream& vecstr) 
{
    vector< complex<double> > v;
    complex<double> num;
    char c;

    vecstr >> c;
    if (c != '[') {
        throw runtime_error("Invalid string [ bracket");
    }
    while (!vecstr.eof()) {

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            return v;
        }

        vecstr >> num;

        if (vecstr.fail() || vecstr.bad()) {
            throw runtime_error("Invalid string");
        }
        v.push_back(num);

        c = vecstr.peek();
        if (c == ']' || c == 0) {
            vecstr >> c; // flush it from istringstream
            break;
        }
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("parse_complex_vector error");
    }
    if (debug) {
        cout << "parse_complex_vector result: ";
        for (int i=0; i<(int)v.size(); i++) {
            cout << v[i] << " ";
        }
        cout << endl;
    }
    return v;
}

// matrices:
ublas::matrix< complex<double> > parse_complex_matrix(istringstream& vecstr) 
{
    ublas::matrix< complex<double> > m;
    vecstr >> m;
    if (debug) {
        cout << "parse_complex_matrix result:" << m << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("error parse_complex_matrix");
    }
    return m;
}

ublas::matrix<double> parse_double_matrix(istringstream& vecstr) 
{
    ublas::matrix<double> m;
    vecstr >> m;
    if (debug) {
        cout << "parse_double_matrix result:" << m << endl;
    }
    if (vecstr.fail() || vecstr.bad()) {
        throw runtime_error("error parse_double_matrix");
    }
    return m;
}

string i2str(int number)
{
   stringstream strs;
   strs << number;
   return strs.str();
}

vector<int> parse_instances_to_number(const string instances)
{
    vector<int> ins;

    try {
        ins.push_back(stoi(instances, nullptr));
    } catch (const invalid_argument& ia) {
        istringstream vecstr(instances);
        ins = parse_int_vector_range(vecstr);
    }
    return ins;
}

