/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . parsers
* *********************************************************************** */

#ifndef PARSE_H
#define PARSE_H 1

#include <string>
#include <vector>
#include <complex>
#include <sstream>

#include <boost/numeric/ublas/matrix.hpp>


int parse_int(std::istringstream& vecstr);
int parse_uint_nothrow(std::istringstream& vecstr);

double parse_double(std::istringstream& vecstr);
double parse_double_nothrow(std::istringstream& vecstr);

std::complex<double> parse_complex_double(std::istringstream& vecstr);

// vectors:
std::vector<int> parse_int_vector(std::istringstream& vecstr);
std::vector<int> parse_int_vector_range(std::istringstream& vecstr);
std::vector<uint> parse_uint_vector_range(std::istringstream& vecstr);
std::vector<int64_t> parse_int64_vector_range(std::istringstream& vecstr);

std::vector<double> parse_double_vector(std::istringstream& vecstr);
std::vector< std::complex<double> > parse_complex_vector(std::istringstream& vecstr);

// matrices:
boost::numeric::ublas::matrix<double> parse_double_matrix(std::istringstream& vecstr);
boost::numeric::ublas::matrix< std::complex<double> > parse_complex_matrix(std::istringstream& vecstr);
std::string i2str(int number);

std::vector<int> parse_instances_to_number(const std::string instances);

#endif
