/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . 
* *********************************************************************** */

#include <cmath>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <sys/time.h>

#include "util.h"

using namespace std;

unsigned int next_power_of_2(unsigned int v)
{
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}

unsigned int ceil_log2(unsigned int val) {
/*
>>> math.ceil(math.log(6,2))
3.0
>>> math.ceil(math.log(8,2))
3.0
>>> math.ceil(math.log(3,2))
2.0
>>> math.ceil(math.log(1,2))
0.0
>>> math.ceil(math.log(2,2))
1.0
>>> math.ceil(math.log(16,2))
4.0
>>> math.ceil(math.log(17,2))
5.0
>>> 2**0
1

>>> 2**3
8
*/
    return((unsigned int)ceil(log2(val)));
}

unsigned int ceil_div(unsigned int num, unsigned int den)
{
    return (unsigned int)ceil((float)num / (float)den);
}


bool my_unique_function (uint i, uint j) {
    return (i==j);
}
vector<uint> unique_and_sort(vector<uint> v)
{
    vector<uint> new_v;
    new_v.resize(v.size());
    vector<uint>::iterator it;
    it = unique_copy(v.begin(),v.end(),new_v.begin());
    sort(new_v.begin(),it);
    // using predicate comparison:
    it = unique_copy(new_v.begin(), it, new_v.begin(), my_unique_function);
    // chop the empty tail off
    new_v.resize( it - new_v.begin() );
    return new_v;
}

#define TIME_STR_LEN 20

void Tictoc::tic(string idstr)
{
    name = idstr;
    // cout << "tic: start [" << name << "]" << endl;
    c_start = clock();
    t_start = chrono::high_resolution_clock::now();
}

void Tictoc::toc(void)
{
    clock_t c_end = clock();
    auto t_end = chrono::high_resolution_clock::now();
    
    struct timeval tv;
    gettimeofday(&tv, NULL);
    time_t secs = tv.tv_sec;
    struct tm * now = gmtime(&secs);
    char time_str[TIME_STR_LEN];
    sprintf(time_str, "%02d:%02d:%02d.%06ld", now->tm_hour, now->tm_min, now->tm_sec, tv.tv_usec);
    
    cout << "[" << time_str << " (UTC)] ";
    cout << "toc-tic (" << name << ") CPU time: " << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms. ";
    cout << "Real time: "
              << chrono::duration_cast<chrono::milliseconds>(t_end - t_start).count()
              << " ms." << endl;
}

void probe(string s) { cout << "probe:" << s << endl; }

vector<vector<uint>> transpose(vector<vector<uint>> Rin)
{
    vector<vector<uint>> RinT;

    // assuming Rin[rows][cols]
    RinT.resize(Rin[0].size());

    for (uint i=0; i<RinT.size(); i++) 
        RinT[i].resize(Rin.size());

    for (uint i=0; i<RinT.size(); i++) { // rows
        for (uint j=0; j<RinT[0].size(); j++) { // cols
            RinT[i][j] = Rin[j][i];
        }
    }
    return RinT;
}

bool find_in_vector(vector<uint>& V, uint p)
{
    for (auto v : V) { 
        if (v == p) {
            return true;
        }
    }
    return false;
}

