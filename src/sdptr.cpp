/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . main program
* *********************************************************************** */

#include <iostream>
#include <exception>
#include <sstream>
#include <fstream>
#include <list>
#include <stdexcept>
#include <thread>

#include <cstring>
#include <cstdlib>
#include <csignal>
#include <sys/time.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include "sdptr.h"
#include "config_reader.h"
#include "tools/parse.h"
#include "opcua/ua_server.h"

using namespace std;

#define SECONDS_PER_TICK 1

/* Global var */
volatile bool ServerRunning = true;
int debug = 0;
Serverdat SD;

void monitor()
{
    TermOutput termout;
    struct timeval current_time;
    time_t secs;
    struct tm * now;
    char time_str[50];

    SD.timetick = SECONDS_PER_TICK;
    clock_gettime(CLOCK_REALTIME, (struct timespec *)&SD.t0);
    pthread_mutex_init(&SD.newpoint_lock, NULL);
    pthread_cond_init(&SD.newpoint_cond, NULL);

    SD.uptime = 0;
    
    gettimeofday(&current_time, NULL);
    SD.start_time = current_time.tv_sec;


    while (ServerRunning) {
        SD.t0.tv_sec = SD.t0.tv_sec + SD.timetick;  // specify next position in time
        SD.t0.tv_nsec = 10000000L;  // 0..999999999 // offset 10ms in a new second
        pthread_mutex_lock(&SD.newpoint_lock);
        pthread_cond_timedwait(&SD.newpoint_cond, &SD.newpoint_lock, (const struct timespec *)&SD.t0);
        pthread_mutex_unlock(&SD.newpoint_lock);
        SD.uptime++;

        gettimeofday(&current_time, NULL);
        secs = current_time.tv_sec;
        now = gmtime(&secs);
        sprintf(time_str, "%02d:%02d:%02d.%06ld", now->tm_hour, now->tm_min, now->tm_sec, current_time.tv_usec);

        cout << "[" << time_str << " (UTC)] " << "MONITOR THREAD: uptime=" << SD.uptime << " seconds" << endl;

        if (SD.unb != NULL) {
            // cout << "sdptr_monitor start" << endl;
            SD.unb->monitor(termout);
            termout.clear();
        }
    }
}

void server_init(bool warm_start)
{
    cout << "read sdptr config file '" << SD.configfile << "'" << endl;
    ConfigReader* p = ConfigReader::getInstance();  // create object of the class ConfigReader
    p->parseFile(SD.configfile);  // parse the configuration file
    p->dumpFileValues();  // print map on the console after parsing it
    
    p->getValue(SD.ant_band_station_type, "n_fpgas", SD.n_fpgas);
    p->getValue(SD.ant_band_station_type, "first_fpga_nr", SD.first_fpga_nr);
    p->getValue(SD.ant_band_station_type, "n_beamsets", SD.n_beamsets);
    p->getValue(SD.ant_band_station_type, "ip_prefix", SD.ip_prefix);
    
    if (SD.n_fpgas == 0) {
        cerr << "ERROR, no settings found for '" << SD.ant_band_station_type << "'" <<endl;
        exit(EXIT_FAILURE);
    }

    list<class NODE_config> NC;
    for (int i=0; i<SD.n_fpgas; i++) {
        NODE_config nc;
        nc.gn = SD.first_fpga_nr + i;
        int32_t board_nr = (int32_t)(nc.gn / FPGAS_PER_BOARD);
        int32_t fpga_nr = (int32_t)(nc.gn % FPGAS_PER_BOARD);
        nc.ipaddr = SD.ip_prefix + to_string(board_nr) + "." + to_string(fpga_nr+1);
        nc.n_beamsets = SD.n_beamsets;
        NC.push_back(nc);
    }

    for (auto nc : NC) {
        cout << nc.gn << ", " << nc.ipaddr << ", " << nc.n_beamsets << endl;
    }

    cerr << "done, it is mine. Now (re)init all" << endl;

    if (warm_start) {
        if (SD.unb != NULL) delete SD.unb;
        SD.unb = NULL;
    }

    list<class Node*> nodelist;
    for (auto nc : NC) {
        uint gn = (uint)nc.gn;
        uint uniboardnr = GLOBALNODE_to_UNB(gn);
        uint n = GLOBALNODE_to_NODE(gn);
        string type = GLOBALNODE_to_TYPE(gn);

        Node *node = new Node(nc.ipaddr, uniboardnr, n, type, nc.n_beamsets);
        nodelist.push_back(node);
    }
    SD.unb = new UniboardMap(nodelist, SD.n_beamsets);

    ua_server_init(warm_start);

    SD.busy = false;
    cout << "done" << endl;
}

static void stopHandler(int signal)
{
    cerr << "received ctrl-c: exit" << endl;
    ServerRunning = false;
    usleep(2000000);
    exit(0);
}

static void hupHandler(int signal)
{
    cerr << "received HUP signal: reload" << endl;
    server_init(true);
}

int main (int argc, char* argv[])
{
    // defaults:
    //bool nodaemon = false;
    bool nodaemon = true;
    debug=0;

    SD.configfile = "/etc/sdptr.conf";
    SD.ant_band_station_type = "-";
    
    // set default values
    SD.n_fpgas = 16;
    SD.first_fpga_nr = 0;
    SD.n_beamsets = 1;
    SD.ip_prefix = "10.99.";

    atomic<size_t> ncthreads(0);
    thread *monitor_thread;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h",      "shows this help text")
        ("version,v",   "prints sdptr version info")
        ("nodaemon",    po::value<bool>(&nodaemon)->zero_tokens(),
                        "With this flag, sdptr runs NOT as daemon")
        ("debug",       po::value(&debug), "Prints out debug info (default=0)")
        ("configfile",  po::value<string>(&SD.configfile)->default_value(SD.configfile),
                        "Specify uniboard configuration file location")
        ("type",        po::value<string>(&SD.ant_band_station_type)->default_value(SD.ant_band_station_type),
                        "Specify antenna band and station type")
    ;

    try {
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            cout << "Usage: " << argv[0] << " [options]" << endl;
            cout << desc;
            exit(EXIT_SUCCESS);
        }
        if (vm.count("version")) {
            cout << argv[0] << ' ' << SDPTR_VERSION << endl;
            exit(EXIT_SUCCESS);
        }
        if (debug) {
            cout << "Debug printing enabled. Level=" << debug << endl;
        }
        if (SD.ant_band_station_type == "-") {
            cout << "need --type see sdptr.conf for accepted [keys]" << endl;
            exit(EXIT_FAILURE);
        }
    } catch (po::error& e) {
        cerr << "ERROR: " << e.what() << endl << endl;
        cerr << desc;
        exit(EXIT_FAILURE);
    } catch (...) {
        cerr << "Exception of unknown type!" << endl;
        exit(EXIT_FAILURE);
    }

    signal(SIGINT, stopHandler);
    //signal(SIGTERM, stopHandler);
    signal(SIGHUP, hupHandler);

    try {
        /*  Open syslog connection */
        //openlog(basename(argv[0]), 0, LOG_LOCAL0);
        //syslog(LOG_INFO,"Starting server %s\n", SDPTR_VERSION);

        if (!nodaemon) {
            if (daemon(1, 0) < 0) cerr << "Error fork as daemon: " << strerror(errno) << endl;
        }

        SD.tr = new TranslatorMap();
        server_init(false);

        monitor_thread = new thread(monitor);
        ua_server();

        monitor_thread->join();
        delete monitor_thread;
        //closelog(); // close syslog connection
        delete SD.unb;
        delete SD.tr;

        exit(EXIT_SUCCESS);
    } catch (exception& e) {
        cerr << "Error server: " << e.what() << endl;
        //syslog(LOG_ERR,"Error server: %s\n", e.what());
        //closelog(); // close syslog connection
        exit(EXIT_FAILURE);
    }
}
