/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . used constants
* *********************************************************************** */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define FPGAS_PER_BOARD 4

#define C_S_pn 12
#define C_W_adc 14
#define C_F_adc 200E6

#define C_WG_MODE_OFF  0
#define C_WG_MODE_CALC 1
#define C_WG_AMPL_UNIT 65536  // 2^16
#define C_WG_PHASE_UNIT (65536.0 / 360.0)  // 2^16 / 360
#define C_WG_FREQ_UNIT 2147483648  // 2^31

#define C_BSN_LATENCY 20000  // 1 period = 5.12us, 20000 = +/- 100ms

#define C_Q_fft 2
#define C_N_sub 512
#define C_N_sub_bf 488
#define C_N_step 1
#define C_N_crosslets_max 7
#define C_N_scrap 512  // Number of 32 bit words in FPGA scrap memory.
#define C_N_pol 2  // Number of antenna polarizations, X and Y.
#define C_A_pn 6  // Number of dual polarization antennas per Processing Node (PN) FPGA.
// #define C_N_beamlets 976  // Number of beamlets per antenna band  488, 976

#define C_200MHZ_1_CNT_NS 5  // Time of one cnt in nS
#define C_N_CLK_PER_PPS 200000000
#endif
