/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . holds information about this program
* *********************************************************************** */

#ifndef _REENTRANT
#error ACK! You need to compile with _REENTRANT defined since this uses threads
#endif

#include <iostream>
#include <exception>
#include <csignal>

#include "tr.h"
#include "sdptr.h"
#include "constants.h"

using namespace std;

// Everything addressed with TR_...
extern int debug;

extern Serverdat SD;

TranslatorMap::TranslatorMap()
{
    translatorMap = new CPointMap();

    translatorMap->add_register("TR_fpga_mask_R",                "-", SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_fpga_mask_RW",               "-", SD.n_fpgas, 1, "RW", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_software_version_R",         "-", 1,          1, "RO", REG_FORMAT_STRING);
    translatorMap->add_register("TR_start_time_R",               "-", 1,          1, "RO", REG_FORMAT_INT64);
    // translatorMap->add_register("TR_tod_client_status_R",        "-", 1,       1, "RO", REG_FORMAT_BOOLEAN);  // TODO: PTP client is part of linux, can I get a status?
    translatorMap->add_register("TR_tod_R",                      "-", 1,          2, "RO", REG_FORMAT_INT64);
    translatorMap->add_register("TR_tod_pps_delta_R",            "-", 1,          1, "RO", REG_FORMAT_DOUBLE);
    translatorMap->add_register("TR_fpga_communication_error_R", "-", SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    // translatorMap->add_register("TR_reload_RW",                  "-", 1,      1, "RW", REG_FORMAT_BOOLEAN);  // Maybe for future.
    translatorMap->add_register("TR_sdp_config_first_fpga_nr_R", "-", 1,          1, "RO", REG_FORMAT_UINT32);
    translatorMap->add_register("TR_sdp_config_nof_fpgas_R",     "-", 1,          1, "RO", REG_FORMAT_UINT32);
    translatorMap->add_register("TR_sdp_config_nof_beamsets_R",  "-", 1,          1, "RO", REG_FORMAT_UINT32);
}

TranslatorMap::~TranslatorMap()
{
    if (translatorMap != NULL) {
        delete translatorMap;
    }
}

bool TranslatorMap::translator(TermOutput& termout, const char cmd, const string addr,
                               const char *data, const int nvalues)
{
    bool retval = false;
    termout.clear();

    try {
        if (cmd == 'R') translatorMap->getReadPermission(addr);
        else if (cmd == 'W') translatorMap->getWritePermission(addr);
    }
    catch (exception& e) {
        cerr << "Tr::point: " << addr << " error: " << e.what() << endl;
        return false;
    }

    int format = (int)translatorMap->getFormat(addr);
    uint32_t n_nodes = translatorMap->getNodesSize(addr);
    uint32_t n_values = translatorMap->getDataSize(addr);
    termout.nof_vals = n_nodes * n_values;
    termout.datatype = format;

    retval = true;

    // reload is not used now, but maybe needed in future
    // if (addr == "TR_reload_W") {
    //     if (cmd == 'W') {
    //         cout << "Tr: tr_reload_W: initiate RELOAD now" << endl;
    //         raise(SIGHUP);
    //     }
    // }
    // else 
    if (addr == "TR_fpga_mask_R") {
        bool *ptr_out = (bool *)termout.val;
        vector<bool> masked = SD.unb->get_all_masked_nodes();
        for (auto val : masked) {
            *ptr_out = val;
            ptr_out++;
        }
    }
    else if (addr == "TR_fpga_mask_RW") {
        if (cmd == 'W') {
            bool *ptr_in = (bool *)data;
            vector<bool> masked;
            for (uint i=0; i<n_nodes; i++) {
                masked.push_back(*ptr_in);
                ptr_in++;
            }
            SD.unb->set_all_masked_nodes(masked);
        }
        else {
            bool *ptr_out = (bool *)termout.val;
            vector<bool> masked = SD.unb->get_all_masked_nodes();
            for (auto val : masked) {
                *ptr_out = val;
                ptr_out++;
            }
        }
    }
    else if (addr == "TR_software_version_R") {
        strcpy(termout.val, SDPTR_VERSION);
    }
    else if (addr == "TR_tod_R") {
        struct timespec ts;
        int64_t *ptr_out = (int64_t *)termout.val;
        clock_gettime(CLOCK_REALTIME, (struct timespec *)&ts);
        ptr_out[0] = (int64_t)ts.tv_sec;
        ptr_out[1] = (int64_t)ts.tv_nsec;
    }
    else if (addr == "TR_tod_pps_delta_R") {
        double *ptr_out = (double *)termout.val;
        vector<uint32_t> pps_offset_cnt = SD.unb->get_all_pps_offset_cnt();
        double pps_delta = 0.0;
        for (uint i=0; i<pps_offset_cnt.size(); i++) {
            if (pps_offset_cnt[i] > 0) {
                cout << "use pps_offset_cnt from node " << i << " cnt=" << pps_offset_cnt[i] << endl;
                pps_delta = (double)(pps_offset_cnt[i] * C_200MHZ_1_CNT_NS) / 1e9;  // pps_delta in seconds now
                break;
            }
        }
        *ptr_out = pps_delta;
    }
    else if (addr == "TR_start_time_R") {
        uint32_t *ptr_out = (uint32_t *)termout.val;
        *ptr_out = (uint32_t)SD.start_time;
    }
    else if (addr == "TR_fpga_communication_error_R") {
        bool *ptr_out = (bool *)termout.val;
        vector<bool> offline = SD.unb->get_all_offline_nodes();
        for (auto val : offline) {
            *ptr_out = val;
            ptr_out++;
        }
    }
    else if (addr == "TR_busy_R") {
        bool *ptr_out = (bool *)termout.val;
        *ptr_out = (bool)SD.busy;
    }
    else if (addr == "TR_uptime_R") {
        uint32_t *ptr_out = (uint32_t *)termout.val;
        *ptr_out = (uint32_t)SD.uptime;
    }
    else if (addr == "TR_sdp_config_first_fpga_nr_R") {
        uint32_t *ptr_out = (uint32_t *)termout.val;
        *ptr_out = (uint32_t)SD.first_fpga_nr;
    }
    else if (addr == "TR_sdp_config_nof_fpgas_R") {
        uint32_t *ptr_out = (uint32_t *)termout.val;
        *ptr_out = (uint32_t)SD.n_fpgas;
    }
    else if (addr == "TR_sdp_config_nof_beamsets_R") {
        uint32_t *ptr_out = (uint32_t *)termout.val;
        *ptr_out = (uint32_t)SD.n_beamsets;
    }
    else {
        retval = false;
    }

    return retval;
}

bool TranslatorMap::read(TermOutput& termout,
                         const string addr, const int nvalues)
{
    return translator(termout, 'R', addr, NULL, nvalues);
}

bool TranslatorMap::write(TermOutput& termout,
                          const string addr, const char *data,
                          const int nvalues)
{
    return translator(termout, 'W', addr, data, nvalues);
}
