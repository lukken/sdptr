/* *************************************************************************
* Copyright 2020
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */

#ifndef UNIBOARD_MAP_H
#define UNIBOARD_MAP_H 1

#include <string>
#include <vector>
#include <list>
#include <stdexcept>

#include "tools/util.h"
#include "board.h"
#include "fpga.h"


class UniboardMap : public Board, public Fpga
{
private:
    bool initialized;
    // std::vector<uint> Subband_mapping; // ...

public:

UniboardMap(std::list<class Node*>& nodelist, const int32_t n_beamsets);
~UniboardMap() {};

bool is_initialized(void) { return initialized; }
bool init(TermOutput& termout);

bool write(TermOutput& termout, const std::string addr,
           const char *data, const int nvalues);

bool read(TermOutput& termout, const std::string addr,
          const int nvalues);

};
#endif
