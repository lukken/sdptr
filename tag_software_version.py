#! /usr/bin/python3

import subprocess
import time


tag_str = time.strftime("%Y-%m-%dT%H.%M.%S_sdptr", time.gmtime())
p1 = subprocess.Popen('git tag -a {} -m "production version"'.format(tag_str), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
print(p1.communicate()[0])

p2 = subprocess.Popen('git describe --tags --always', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

version = p2.communicate()[0].decode('utf-8')
print(version)


with open('config.h', 'r') as fd:
    config_file = fd.read()

ptr = config_file.find('#define VERSION')
ptr1 = config_file.find('"', ptr) + 1
ptr2 = config_file.find('"', ptr1)

old_version = config_file[ptr1:ptr2]
config_file = config_file.replace(old_version, version.strip())

with open('config.h', 'w') as fd:
    fd.write(config_file)
