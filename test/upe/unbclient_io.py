#!/usr/bin/env python

import time
import socket
import struct

class UNBclientIO():
    def __init__(self, serverhost, tcpport=3335):

        self.myName = 'UNBclientIO'
        self.pId = self.myName + ' - '
        self.tcpport = tcpport
        self.serverhost = serverhost
        self.serverhost = 'localhost'

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.serverhost, self.tcpport))
        
        (ret,banner) = self.read_section('output')
        self.read_prompt('')
        print("Connected to unbserver on: ",self.serverhost," (tcpport=",self.tcpport,")")
        print("Received banner: ",banner)

    def __del__(self):
        self.s.close()

    def verify_section_name(self,buf,section_name):
        ret=False
        result=buf
        #print("verify_section_name=",result)
        col=buf.split('=')
        verifyname=col[0].strip('\n')
        
        if verifyname == section_name:
            ret=True
            result=buf.strip('}')
            col=result.split('{')
            result=col[1]
        return (ret,result)

    def prefixok(self,buf):
        col=buf.split(':')
        prefix=col[0].strip('\n')
        return prefix == 'sdptr'

    def cmdname(self,buf):
        col=buf.split(':')
        return col[1]

    def cmdok(self,buf):
        col=buf.split(':')
        print("cmdok=",col[2])
        return col[2] == '0'

    def read_until_eos(self,eos):
        new_in = 0
        can_read = 0
        while new_in==0:
            buf = self.s.recv(600000, socket.MSG_PEEK)
            if not buf:
                print('server is disconnected')
                break
            else:
                i=0
                for c in buf:
                    if chr(c)==eos:
                        can_read=i+1
                        new_in=1
                        break
                    else:
                        i+=1

        buf = self.s.recv(can_read)
        print('received:',buf)
        return buf.decode('utf-8')

    def read_section(self,section):
        result_found=False
        response=''
        while result_found==False:
            buf=self.read_until_eos('}')
            if not buf:
                print('server is disconnected')
                result_found=True
                break
            else:
                (result_found,response)=self.verify_section_name(buf,section)
                if result_found==False:
                    print('Unexpected Response of section=',section)

        return (result_found,response)

    def read_prompt(self,cmdname):
        result_found=False
        response=''
        while result_found==False:
            buf=self.read_until_eos('>')
            if not buf:
                print('server is disconnected')
                result_found=True
                break
            else:
                if self.prefixok(buf) == False:
                    print('Unexpected Response, Prefix NOT OK')
                else:
                    if self.cmdname(buf) == cmdname:
                        result_found=True
        return self.cmdok(buf)

    def find_response(self, cmdname=''):
        (ret,out) = self.read_section('output')

        (ret,err) = self.read_section('errors')
        if cmdname != '':
            ret=self.read_prompt(cmdname)
        return (ret,out,err)

#==================================================================================
    def nodes_to_unb_pn(self, nodes):
        unbs=[]
        pns=[]
        for n in nodes:
            unbs.append(n >> 3)
            pns.append(n % 4)
        u_unbs = list(set(unbs)) # use only unique values
        u_pns = list(set(pns))
        return (u_unbs, u_pns)

    def mwrite(self, nodes, addrname, offset, data):
        cmdname='write'
        ret=False

        (unbs,pns) = self.nodes_to_unb_pn(nodes)

        cmd  = cmdname + ' --offs=' + str(offset)
        cmd += ' --data=' + str(data).replace(" ","")
        cmd += ' /unb' + str(unbs).replace(" ","") + '/pn' + str(pns).replace(" ","")
        cmd += '/mm/' + addrname

        print("send:",cmd)
        my_cmd_as_bytes = str.encode(cmd + '\n')
        self.s.send(my_cmd_as_bytes)
        (ret,out,err) = self.find_response(cmdname)
        err=err.strip('\n')
        print('err=',err)
        print('out=',out)
        print('ret=',ret)
        return (ret,out,err.strip('\n'))

    def mread(self, nodes, addrname, offset, nvalues):
        cmdname='read'
        ret=False

        (unbs,pns) = self.nodes_to_unb_pn(nodes)

        cmd  = cmdname + ' --nvalues=' + str(nvalues)
        cmd += ' --offs=' + str(offset)
        cmd += ' /unb' + str(unbs).replace(" ","") + '/pn' + str(pns).replace(" ","")
        cmd += '/mm/' + addrname

        print("send:",cmd)
        my_cmd_as_bytes = str.encode(cmd + '\n')
        self.s.send(my_cmd_as_bytes)
        (ret,out,err) = self.find_response(cmdname)
        err=err.strip('\n')
        print('err=',err)
        print('out=',out)
        print('ret=',ret)
        if ret == True:
            node_and_value=eval(out)
        else:
            node_and_value=[]
            for n in nodes:
                node_and_value.append((n,1,[])) # FIXME: sdptr server better do this
        return (ret,node_and_value,err.strip('\n'))

