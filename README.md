If you use (part of) this software please attribute the use to ASTRON as indicated in the
file NOTICE in the root of the source code repository.



# In brief:

in case of missing automake/ autoconf:
```
sudo apt install autoconf
sudo apt install automake
```

in case of missing boost libraries:

`sudo apt-get install libboost-dev`
(or even `sudo apt-get install libboost-all-dev`)




# Compile instructions:

compile from scratch:
```
autoreconf -f -i
./configure
make
```


# Configure server if needed:
vi src/sdptr.conf 

Usage:
Startup server process:
(for testing)
```
cd src
./sdptr --type=LTS --configfile=sdptr.conf --nodaemon
```


# OPC ua

## Python C library install
(note: already installed on dop36)

git clone https://github.com/open62541/open62541
follow install instructions from https://open62541.org/doc/current/installing.html

  194  cd
  195  mkdir opcua
  196  cd opcua/
  200  cd open62541/
  203  sudo apt-get install git build-essential gcc pkg-config cmake python
  204  sudo apt-get install cmake-curses-gui # for the ccmake graphical interface
  205  sudo apt-get install libmbedtls-dev # for encryption support
  206  sudo apt-get install check libsubunit-dev # for unit tests
  207  sudo apt-get install python-sphinx graphviz # for documentation generation
  208  sudo apt-get install python-sphinx-rtd-theme # documentation style
  210  cd build/
  212  rm -r *
  215  cmake ..
  216  make
  218  sudo make install


## Python Client Install
(note: already installed on dop36)
```
sudo apt install python-opcua
sudo apt install python-opcua-tools
cd opcua/
git clone https://github.com/FreeOpcUa/python-opcua.git
```

## Test the OPCua client with the proviced C client

Compile the OPCua client: 
`cd test/c/ua_client`
then execute the command shown in the README file in that directory

1. Start the sdptr server on dop36:
Default the config file `sdptr.conf` is read from `/etc/` directory, and can be changed using key `--configfile` then the full path is given.
```
cd src
./sdptr --type LTS --configfile=sdptr.conf --nodaemon
```

2. Run the client:
```
cd test/c/ua_client
./client
```

The (test) client reads out all available Uniboard registers.

## Test the OPCua client with the opcua-client (GUI) on test-lcu2.astron.nl

`ssh -J hiemstra@portal.astron.nl -L localhost:55555:test-lcu2.astron.nl:55555 hiemstra@test-lcu2.astron.nl`
run OPCua GUI client:
`opcua-client`
then open an OPCua connection to: `opc.tcp://dop36:4840`

